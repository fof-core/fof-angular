import { Component, OnInit, OnDestroy } from '@angular/core'
import { SettingsService } from '../settings.service'
import { Subscription } from 'rxjs'

@Component({
  selector: 'company-settings-config',
  templateUrl: './settings-config.component.html',
  styleUrls: ['./settings-config.component.scss']
})
export class SettingsConfigComponent implements OnInit {
  constructor(
    private readonly settingsService: SettingsService
  ) { }

  // All private variables
  private priVar = {
    themeSub: <Subscription><unknown>undefined,
    // stickyHeaderSub: <Subscription><unknown>undefined,
    pageTransitionSub: <Subscription><unknown>undefined
  }
  // All private functions
  private privFunc = {
  }
  // All variables shared with UI 
  public uiVar = {
    themes: [
      { value: 'theme-red', label: 'Red' },
      // { value: 'light-theme', label: 'Clair' },
      // { value: 'nature-theme', label: 'Nature' },
      { value: 'theme-dark', label: 'Dark' }
    ],
    currentTheme: 'theme-red',
    // currentStickyHeader: true,
    currentPageTransition: true
  }
  // All actions shared with UI 
  public uiAction = {
    onThemeSelect:(value: string) => {
      this.settingsService.theme = value
    },
    // onStickyHeaderToggle:(value: boolean) => {        
    //   this.settingsService.stickyHeader = value
    // }
    onPageTransitionToggle:(value: boolean) => {        
      this.settingsService.pageTransition = value
    }
  }
  // Angular events
  ngOnInit() {
    this.priVar.themeSub = this.settingsService.themeChange
      .subscribe((theme: string) => { this.uiVar.currentTheme = theme })
    // this.priVar.stickyHeaderSub = this.settingsService.stickyHeaderChange
    //   .subscribe((value: boolean) => { this.uiVar.currentStickyHeader = value })
    this.priVar.pageTransitionSub = this.settingsService.pageTransitionChange
      .subscribe((value: boolean) => { this.uiVar.currentPageTransition = value})
  }  
  ngOnDestroy() {
    if (this.priVar.themeSub) { this.priVar.themeSub.unsubscribe() }
    // if (this.priVar.stickyHeaderSub) { this.priVar.stickyHeaderSub.unsubscribe() }
    if (this.priVar.pageTransitionSub) { this.priVar.pageTransitionSub.unsubscribe() }
  }
}
