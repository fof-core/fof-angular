import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { SettingsConfigComponent } from './settings-config/settings-config.component'
import { FofAuthGuard } from '@fof-angular/core'

const routes: Routes = [
  { path: 'settings', component: SettingsConfigComponent, 
    data: { title: 'Configuration' }}
    // , canActivate: [FofAuthGuard]}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
