import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { FofLocalstorageService } from '@fof-angular/core'


@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(
    private fofLocalstorageService: FofLocalstorageService
  ) { 
    let theme = this.fofLocalstorageService.getItem('theme')
    if (theme) {
      this.theme = theme
    } else {
      this.theme = 'theme-red'
    }
    // let stickyHeader = this.fofLocalstorageService.getItem('stickyHeader')
    // this.stickyHeader = stickyHeader
    
    let pageTransition = this.fofLocalstorageService.getItem('pageTransition')
    this.pageTransition = pageTransition
  }

  private _theme: string = 'theme-red'
  private _themeChange: BehaviorSubject<string> = new BehaviorSubject('theme-red')  
  public readonly themeChange: Observable<string> = this._themeChange.asObservable()

  public get theme() {
    return this._theme
  }
  public set theme(theme) {
    this._theme = theme
    this._themeChange.next(this._theme)
    this.fofLocalstorageService.setItem('theme', this._theme)
  }

  // private _stickyHeader: boolean = true
  // private _stickyHeaderChange = new BehaviorSubject(true)  
  // public readonly stickyHeaderChange: Observable<boolean> = this._stickyHeaderChange.asObservable()

  // public get stickyHeader() {
  //   return this._stickyHeader
  // }
  // public set stickyHeader(stickyHeader) {
  //   this._stickyHeader = stickyHeader
  //   this._stickyHeaderChange.next(this._stickyHeader)
  //   this.fofLocalstorageService.setItem('stickyHeader', this._stickyHeader)
  // }

  private _pageTransition = true
  private _pageTransitionChange = new BehaviorSubject(true)  
  public get pageTransitionChange() {
    return this._pageTransitionChange.asObservable()
  }

  public get pageTransition() {
    return this._pageTransition
  }
  public set pageTransition(pageTransition) {
    this._pageTransition = pageTransition
    this._pageTransitionChange.next(this._pageTransition)
    this.fofLocalstorageService.setItem('pageTransition', this._pageTransition)
  }
}
