import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { SettingsRoutingModule } from './settings-routing.module'
import { SettingsConfigComponent } from './settings-config/settings-config.component'

import { MaterialModule } from '../material.module'
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    SettingsConfigComponent
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    FormsModule,
    MaterialModule
  ]
})
export class SettingsModule { }
