import { ModuleWithProviders, NgModule } from '@angular/core'
import { LayoutModule } from './layout/layout.module'
import { MaterialModule } from './material.module'
import { AuthOidcModule } from '@fof-angular/auth-oidc'
import { SettingsModule } from './settings/settings.module'

@NgModule({
  declarations: [
    
  ],
  imports: [
    AuthOidcModule,
    MaterialModule,
    // PermissionsModule,
    LayoutModule,
    SettingsModule,
    // FofCoreModule,
    // CoreModule
    // PermissionsModule
    // FofCoreModule.forRoot({})   
  ],
  exports: [
    LayoutModule
  ]
})
export class CompanyDefaultModule { 
  
}
