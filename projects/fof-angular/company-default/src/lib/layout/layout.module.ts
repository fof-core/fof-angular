import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { MasterPageComponent } from './master-page/master-page.component'
import { BrowserModule } from '@angular/platform-browser'
import { LayoutRoutingModule } from './layout-routing.module'
import { MaterialModule } from '../material.module'
import { NotFoundComponent } from './not-found/not-found.component'
import { FofCoreModule }from '@fof-angular/core'

@NgModule({
  declarations: [
    MasterPageComponent,
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    MaterialModule,    
    LayoutRoutingModule,
    FofCoreModule
  ],
  exports: [
    MasterPageComponent
  ]
})
export class LayoutModule { }
