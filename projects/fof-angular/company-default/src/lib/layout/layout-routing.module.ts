import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
// import { HomepageComponent } from './layout/homepage/homepage.component'
// import { AuthGuard } from './core/auth.guard'
import { NotFoundComponent } from './not-found/not-found.component'


const routes: Routes = [
  // {
  //   path: 'test',
  //   loadChildren: () => import('@fof-angular/core').then(me => me.PermissionsModule)
  // },
  { path: 'toto', component: NotFoundComponent },
  // {
  //   path: 'samples',
  //   loadChildren: () => import('./samples/samples.module').then(m => m.SamplesModule),
  //   canActivate: [AuthGuard]
  // },
  // { path: '**', component: NotFoundComponent }
  // {path: '', component: LayoutPublicHomepageComponent, canActivate: [AuthGuard]}  
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
