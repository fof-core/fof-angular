import { Component, OnInit } from '@angular/core'
import { Observable, of, Subject, Subscription } from 'rxjs'
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout'
import { ActivatedRoute, Router } from '@angular/router'
import { takeUntil } from 'rxjs/operators'
import { OidcSecurityService } from 'angular-auth-oidc-client'
import { UserService } from '@fof-angular/core'
import { OverlayContainer } from '@angular/cdk/overlay'
import { SettingsService } from '../../settings/settings.service'
import { fadeAnimation } from './animations/fade.animation'

@Component({
  selector: 'comp-master-page',
  templateUrl: './master-page.component.html',
  styleUrls: ['./master-page.component.scss'],
  animations: [fadeAnimation]
})
export class MasterPageComponent implements OnInit {

  constructor(
    private oidcSecurityService: OidcSecurityService,  
    private breakpointObserver: BreakpointObserver,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private overlayContainer: OverlayContainer,
    private settingsService: SettingsService,
  ) {
    this.activatedRoute.queryParams
      // Remove subscribtion when destroy$ is fired by ngOnDestroy
      .pipe(takeUntil(this.priVar.destroy$))
      .subscribe(params => {      
        this.priVar.returnUrl = params['returnUrl']      
      })
  }

  // All private variables
  private priVar = {
    returnUrl: <string><unknown>undefined,
    breakpointObserverSub: <Subscription><unknown>undefined,
    themeSub: <Subscription><unknown>undefined,
    destroy$: new Subject<void>()
  }
  // All private functions
  private privFunc = {
  }
  // All variables shared with UI 
  public uiVar = {
    sideNavOpened: false,  
    sideNavMode: 'side',  
    theme$: <Observable<string>> of('default-theme'),
    loading: true,
    isSmallDevice: false,
    currentUser: <any><unknown>undefined,
    pageTransition: true,    
  }
  // All actions shared with UI 
  public uiAction = {
    login:() => {
      this.oidcSecurityService.authorize()
    },  
    logOut: () => {      
      this.oidcSecurityService.logoff()
    },
    getRouterOutletState:(outlet: any) => {      
      if (outlet.isActivated && this.uiVar.pageTransition) {
        return outlet.activatedRoute
      }
      return ''
    }
  }
  // Angular events
  ngOnInit() {
    this.priVar.themeSub = this.settingsService.themeChange 
      .pipe(takeUntil(this.priVar.destroy$))     
      .subscribe((theme: string) => {
        if (theme) {
          this.uiVar.theme$ = of(theme)

          //toDo: responsive to theme change
          const classList = this.overlayContainer.getContainerElement().classList    
          
          const toRemove = Array.from(classList).filter((item: string) =>
            item.includes('theme-')
          )
          if (toRemove.length) {
            classList.remove(...toRemove)
          }
          classList.add(theme)
        }
      }) 

    this.settingsService.pageTransitionChange
      .pipe(takeUntil(this.priVar.destroy$))
      .subscribe((value: boolean) => { this.uiVar.pageTransition = value })

    this.breakpointObserver.observe(Breakpoints.XSmall)
      .pipe(takeUntil(this.priVar.destroy$))
      .subscribe((state: BreakpointState) => {      
        if (state.matches) {
          // XSmall
          this.uiVar.isSmallDevice = true
          this.uiVar.sideNavOpened = false
        } else {
          // > XSmall
          this.uiVar.isSmallDevice = false        
          this.uiVar.sideNavOpened = true
        }
      }) 

    this.oidcSecurityService.checkAuth()
      .pipe(takeUntil(this.priVar.destroy$))
      .subscribe((isAuthentified) => {              
        if (isAuthentified) {          
          this.userService.getCurrentUser().toPromise()
          .then(user => {
            console.log('USER', user)
            this.uiVar.currentUser = user 
            if (this.priVar.returnUrl) { 
              // reload             
              this.router.navigateByUrl(this.priVar.returnUrl)
            }            
            this.uiVar.loading=false      
          })
        } else {
          this.oidcSecurityService.authorize()
        }
      })
  } 
  
  ngOnDestroy() {    
    // Ask to remove the subscribtions
    this.priVar.destroy$.next()    
  }

}
