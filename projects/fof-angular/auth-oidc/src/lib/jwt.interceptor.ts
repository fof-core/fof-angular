import { Injectable } from '@angular/core'
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http'
import { Observable } from 'rxjs'
import { OidcSecurityService } from 'angular-auth-oidc-client'

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  
  constructor(
    private oidcSecurityService: OidcSecurityService
  ) { 
    
  }

  private bearerToken: string = ''

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    const isApiUrl = true

    if (!this.bearerToken) {
      // check 'til the user is authentified
      // toDo: to change
      this.bearerToken = this.oidcSecurityService.getToken()
    }
    
    if (this.bearerToken && isApiUrl) {
      request = request.clone({         
        setHeaders: {
          Authorization: `Bearer ${this.bearerToken}`          
        }
      })
    } else {        
      request = request           
    }    

    return next.handle(request)
  }
}
