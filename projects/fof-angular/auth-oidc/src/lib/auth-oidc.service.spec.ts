import { TestBed } from '@angular/core/testing';

import { AuthOidcService } from './auth-oidc.service';

describe('AuthOidcService', () => {
  let service: AuthOidcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthOidcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
