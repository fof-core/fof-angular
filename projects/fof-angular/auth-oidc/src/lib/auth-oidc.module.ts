import { APP_INITIALIZER, NgModule } from '@angular/core'
import { JwtInterceptor } from './jwt.interceptor'
// OIDC
import { AuthModule, OidcConfigService } from 'angular-auth-oidc-client'
import { HTTP_INTERCEPTORS } from '@angular/common/http'

function configureAuth(oidcConfigService: OidcConfigService): () => Promise<any> {
  return () =>
    oidcConfigService.withConfig({
      stsServer: 'https://login.microsoftonline.com/984d4104-09b9-42e5-a80f-a48a1430c929/v2.0',
      authWellknownEndpoint: 'https://login.microsoftonline.com/984d4104-09b9-42e5-a80f-a48a1430c929/v2.0/.well-known/openid-configuration',                        
      redirectUrl: window.location.origin,
      clientId: 'eb3e5b4e-c847-4a1b-9f47-51c55da9e577',      
      scope: 'openid profile offline_access api://eb3e5b4e-c847-4a1b-9f47-51c55da9e577/CvGen.Access',       
      responseType: 'code',
      silentRenew: true,
      useRefreshToken: true,
      maxIdTokenIatOffsetAllowedInSeconds: 600,
      issValidationOff: false,
      autoUserinfo: false      
  });
}

@NgModule({
  imports: [AuthModule.forRoot()],
  exports: [AuthModule],
  providers: [
    OidcConfigService,
    // AuthService,
    {
      provide: APP_INITIALIZER,
      useFactory: configureAuth,
      deps: [OidcConfigService],
      multi: true,
    },
   { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
})
export class AuthOidcModule { }
