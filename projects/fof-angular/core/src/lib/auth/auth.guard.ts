import { Injectable, Inject } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router'
import { Observable } from 'rxjs'
import { UserService } from './user.service'

@Injectable({
  providedIn: 'root'
})
export class FofAuthGuard implements CanActivate {

  constructor(
    private router: Router,        
    private userService: UserService
  ) { 
  }

  canActivate(
    next: ActivatedRouteSnapshot,    
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // for permissions in route 
    // https://jasonwatmore.com/post/2019/08/06/angular-8-role-based-authorization-tutorial-with-example#tsconfig-json
    const currentUser = this.userService.user
    let notAuthentifiedRoute = '/'
      

    if (currentUser) {
      // check if route is restricted by permission      
      // get the deepest root data
      // https://stackoverflow.com/questions/43806188/how-can-i-access-an-activated-child-routes-data-from-the-parent-routes-compone
      let route = next; while (route.firstChild) { route = route.firstChild }
      const data = route.data

      // if the route have a permission list associated
      if (data.permissions) {
        // const found = Object.values(currentUser.permissions || []).some((r: string)=> data.permissions.indexOf(r) >= 0)
        // const found = true
        // const found = currentUser.permissions?.includes()
        const found = currentUser.permissions?.some( ai => data.permissions.includes(ai) ) 

        if (!found) {      
          this.router.navigate(['/'])
          return false
        }
      }
      // authorised, no restriction on the route
      return true
    }

    // not logged in. Redirect to then notAuthentified page with the url to return on
    this.router.navigate([notAuthentifiedRoute], { queryParams: { returnUrl: state.url } })
    return false
  }
  
}
