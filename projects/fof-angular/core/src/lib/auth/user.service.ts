import { Inject, Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
// import { environment } from '../../environments/environment'
import { entUser } from './user.entity'
import { map } from 'rxjs/operators'
import { CORE_CONFIG, IFofConfig } from '../fof-config'
import { BehaviorSubject, Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpClient: HttpClient,
    @Inject(CORE_CONFIG) private conf: IFofConfig
  ) {     
    this.currentUserSubject = new BehaviorSubject<entUser>({})
    this.currentUser$ = this.currentUserSubject.asObservable()    
  }

  private _user: entUser|undefined 

  public get user():entUser|undefined   {
    return this._user
  }

  private currentUserSubject!: BehaviorSubject<entUser>
  public currentUser$!: Observable<entUser>

  public getCurrentUser() {
    // return this.httpClient.get(`${environment.apiPath}/users/current`)    
    return this.httpClient.get(`${this.conf.environment.apiPath}/users/current`)
    .pipe(map((user: entUser) => {
      this._user = user
      this.currentUserSubject.next(user)
      return user
    }))
  }
}
