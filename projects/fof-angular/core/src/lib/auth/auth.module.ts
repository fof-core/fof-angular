import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
// import { FofAuthGuard } from './auth.guard'
import { UserService } from './user.service'

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    UserService,
    // FofAuthGuard
  ]
})
export class AuthModule { }
