import { InjectionToken } from '@angular/core'

export interface IfofNavigation {
  url?: string,
  external?: true,
  target?: string,
  label: string,
  icon?: string,
  childs?: IfofNavigation[]
}

/**
 * Interface for fof-core/angular package
 */
export interface IFofConfig {  
  appName: {    
    /** Give the friendly name for your app. */
    long: string,
    /** Will be used in all places where a technical name is necessary. (e.g.  localstorage key root name)*/
    technical: string
  },  
  mainTitle: {
    /** Will be used as main title for devices up to xl*/
    long: string,
    /** Will be used as main title for mobile devices */
    short: string
  }  
  /** An enum with your permission actions 
   * @example
   * export enum eCp = { invoiceCreate = 'invoiceCreate' }    
  */
  permissions?: any,  
  /** your environment object (not the path of the file, the object itself) */
  environment: {
    apiPath: string
  },
  /** 
   * the main navigation with the default label
   * @example
   * navigationMain: [{ link: 'settings', label: 'A propos' }]
   */
  navigationMain: Array<IfofNavigation>,
  /** By default, the authentication is basic */  
}

export const CORE_CONFIG = new InjectionToken(
  'CORE_CONFIG'
)