import { Injectable, Inject } from '@angular/core'
import { IFofConfig, CORE_CONFIG } from './fof-config'

@Injectable({
  providedIn: 'root'
})
export class FofLocalstorageService {

  constructor(
    @Inject(CORE_CONFIG) private fofConfig: IFofConfig
  ) { 
    this.appShortName = `${this.fofConfig.appName.technical.toLocaleUpperCase()}-`
  }

  private appShortName: string

  setItem(key: string, value: any) {
    localStorage.setItem(`${this.appShortName}${key}`, JSON.stringify(value))
  }

  getItem(key: string) {
    const item = localStorage.getItem(`${this.appShortName}${key}`)
    if (item) {
      return JSON.parse(item)
    } 
    return null    
  }

  removeItem(key: string) {
    localStorage.removeItem(`${this.appShortName}${key}`)
  }
}
