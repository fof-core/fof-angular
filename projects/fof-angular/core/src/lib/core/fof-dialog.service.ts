import { Injectable } from '@angular/core'
import { FofCoreDialogYesNoComponent, iInternal, iYesNo, iInformation } from './core-dialog-yes-no/core-dialog-yes-no.component'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'


@Injectable({
  providedIn: 'root'
})
export class FofDialogService {

  constructor(
    private readonly matDialog: MatDialog
  ) { }

  public openYesNo(options: iYesNo) {
    return new Promise((resolve, reject) => {

      let _options: iInternal = <iInternal>options

      let width = options.width || '300px' 
      let height = options.height || undefined

      _options.informationOnly = _options.informationOnly || false

      const dialogRef = this.matDialog.open(FofCoreDialogYesNoComponent, {
        data: _options,        
        width: width,
        height: height,
        // to have a look on, bug
        position: {
          top: '150px'
        }
      })
      dialogRef.afterClosed()
      .toPromise()
      .then(result => {
        resolve(result)
      })
    })
  }

  public openInformation(options: iInformation) {
    return new Promise((resolve, reject) => {

      let _options: iInternal = <iInternal>options
      _options.informationOnly = true
      _options.title = _options.title || 'fof à une information'
      _options.yesLabel = _options.yesLabel || 'ok'

      this.openYesNo(_options)
      .then(result => {
        resolve(result)
      })
    })
  }
}
