import { Injectable } from '@angular/core'
import { iFofHttpException } from './core.interface'
import { FofNotificationService } from './notification/notification.service'

export interface iFofCleanException extends iFofHttpException {
  isConstraintError: boolean
}

@Injectable({
  providedIn: 'root'
})
export class FofErrorService {

  constructor(
    private fofNotificationService: FofNotificationService
  ) { }

  /** Only for the core service, the error should be already cleaned */
  cleanErrorOld (errorToManage: any): iFofCleanException {
    let errorToReturn: iFofCleanException = errorToManage
    let message: string
    let isConstraintError: boolean = false
    
    // console.log('errorToManage', errorToManage)

    if (errorToManage.error instanceof ErrorEvent) {
      // client-side error
      console.log('toDo: manage error')
      console.log('errorToManage', errorToManage)
      return errorToManage
    } else {
      // HTTP or server-side error
      if (errorToManage.error) {
        if (errorToManage.error instanceof Object) {
          if (errorToManage.error.fofCoreException) {
            if (errorToManage.error.message instanceof Object) {
             // it's an http rest error
             const _error = errorToManage.error.message
             if (_error.error) {               
              errorToManage.error.statusText = _error.error
              errorToManage.error.message = _error.message
             }                          
            } else {
              message = errorToManage.error.message
              if (message && message.search('constraint') > -1 ) {
                isConstraintError = true
              }                      
            }          
            return {
              isConstraintError,
              ...errorToManage.error
            }            
          } else {
            if (errorToManage.status == 0) {              
              console.error('FOF-UNKNOWN-ERROR', errorToManage.error)
              errorToManage.error = errorToManage.message
              errorToManage.message = 'Impossible to contact the server'
            }
          }
        } else {
          // the error to manage is comming from the httpClient
          return errorToManage
        }
      }
    }  

    return errorToReturn
  }

  cleanError (errorToManage: any): iFofCleanException {
    return errorToManage
  }

  errorManage(errorToManage: iFofCleanException) {
    let message = errorToManage.message || 'no error message found'
    if (errorToManage.error) {
      message = message + `<br>${errorToManage.error}`
    }
    if (errorToManage.status) {
      switch (errorToManage.status) {
        case 400:
          if (errorToManage.error && errorToManage.error.response) {
            const badRequest = errorToManage.error.response
            if (badRequest.error && badRequest.error.message == "ValidationError") {
              console.log('toDo: manage, magic string etc')
              errorToManage = badRequest.error
            }
          }
          break
        case 401:
        case 403:
          message = `Forbidden`
          break
        default:          
          message = `Oups, There is an error<br>
          <small>${message}</small><br>`          
          break
      }
    }
    console.error('fof error', errorToManage.error)
    this.fofNotificationService.error(message, {mustDisappearAfter: -1})
  }

  errorManageOld(errorToManage: iFofCleanException) {
    let message = errorToManage.message
    if (errorToManage.error) {
      message = message + `<br>${errorToManage.error}`
    }
    if (errorToManage.status) {
      switch (errorToManage.status) {
        case 401:
        case 403:
          message = `Forbidden`
          break
        default:          
          message = `Oups, THere is an error<br>
          <small>${message}</small><br>`          
          break
      }
    }
    // this.fofNotificationService.error(message, {mustDisappearAfter: -1})
  }
}
