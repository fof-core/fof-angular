import { Component, OnInit, ɵNG_INJ_DEF, ViewChild } from '@angular/core'
import { UserService } from '../../auth/user.service'
import { entUser } from '../../auth/user.entity'
import { BaseService } from './base.service'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { Breakpoints, BreakpointState } from '@angular/cdk/layout'

export interface eBaseComponentConfig {
  permissions?: {
    create?: string,
    update?: string,
    delete?: string
  }  
}

export abstract class BaseComponent {
 
  constructor(
    // userService: UserService
    baseService: BaseService,
    config?: eBaseComponentConfig
  ) {

    const permissions = config?.permissions

    if (permissions) {
      baseService.userService.currentUser$
      .pipe(takeUntil(this.base.destroy$))
      .subscribe((user: entUser) => {  
        
        if (permissions.create) {
          if (user.permissions?.includes(permissions.create)) {
            this.base.permissions.create = true
          }     
        }

        if (permissions.update) {
          if (user.permissions?.includes(permissions.update)) {
            this.base.permissions.update = true
          }     
        }

        if (permissions.delete) {
          if (user.permissions?.includes(permissions.delete)) {
            this.base.permissions.delete = true
          }     
        }
      })
    }
      

    baseService.breakpointObserver.observe(Breakpoints.XSmall)
    .pipe(takeUntil(this.base.destroy$))
    .subscribe((state: BreakpointState) => {      
      if (state.matches) {                
        // XSmall
        this.base.isSmallDevice = true        
      } else {        
        // > XSmall
        this.base.isSmallDevice = false        
      }
    }) 
  }

  public base = {
    destroy$: new Subject<void>(),
    permissions: {
      read: false,
      create: false,
      update: false,    
      delete: false,
    },    
    isSmallDevice: false
  }

  public destroy() {
    this.base.destroy$.next()
  }

}