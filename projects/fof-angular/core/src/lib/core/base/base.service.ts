import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout'
import { HttpClient } from '@angular/common/http'
import { Inject, Injectable } from '@angular/core'
import { BehaviorSubject, Observable, of } from 'rxjs'
import { map, takeUntil } from 'rxjs/operators'
import { entUser } from '../../auth/user.entity'
import { UserService } from '../../auth/user.service'
import { Title } from "@angular/platform-browser"

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(
    // private httpClient: HttpClient,
    // @Inject(CORE_CONFIG) private conf: IFofConfig
    public userService: UserService,
    public breakpointObserver: BreakpointObserver,
    public title: Title
  ) { 
   
  }

 
}
