import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FofNotificationService } from './notification/notification.service'
import { FofDialogService } from './fof-dialog.service'
import { MaterialModule } from './material.module'
import { FofErrorService } from './fof-error.service'
import { BaseService } from './base/base.service'
import { FofCoreDialogYesNoComponent } from './core-dialog-yes-no/core-dialog-yes-no.component'
import { notificationComponent } from './notification/notification/notification.component'
// import { BrowserModule } from '@angular/platform-browser'
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
// import { FormsModule, ReactiveFormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    notificationComponent,
    FofCoreDialogYesNoComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    // BrowserModule,
    // BrowserAnimationsModule,
    // FormsModule, 
    // ReactiveFormsModule
  ],
  entryComponents: [
    FofCoreDialogYesNoComponent
  ],
  providers: [
    FofNotificationService,
    FofErrorService,
    FofDialogService,
    BaseService
  ],
  exports: [
    notificationComponent,
    FofCoreDialogYesNoComponent
    // FofNotificationService,
    // FofErrorService,
    // FofDialogService,
  ]
})
export class CoreModule { }
