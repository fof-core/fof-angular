import { FormGroup, FormControl } from '@angular/forms'
import { iFofHttpException } from './core.interface'

// export interface iFofCleanException extends iFofHttpException {
//   isConstraintError: boolean
// }

export const fofUtilsForm = {
  validateAllFields:(formGroup: FormGroup) => {         
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);             
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        fofUtilsForm.validateAllFields(control);            
      }
    })
  }
}

// export const fofError = {
//   clean:(errorToManage: any): iFofCleanException => {
//     let error
//     let message: string
//     let isConstraintError: boolean = false
//     if (errorToManage.error) {
//       error = <iFofHttpException>errorToManage.error
//       if (error.fofCoreException) {
//         message = error.exceptionDetail.message
//         if (message.search('constraint') > -1 ) {
//           isConstraintError = true
//         }
//         return {
//           isConstraintError,
//           ...error
//         }
//       }
//     }
//   }
// }

