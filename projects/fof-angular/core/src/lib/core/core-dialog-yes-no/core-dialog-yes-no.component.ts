// Angular
import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation, 
  AfterViewInit, Inject, Optional} from '@angular/core'
// SPS Services
// SPS Models
// SPS Components
// Vendors & utils
import {MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog'

export interface iInformation {
  title? : string,
  question: string,
  yesLabel? : string,  
  width?: string,
  height?: string  
}

export interface iYesNo extends iInformation  {
  noLabel?: string,
}

export interface iInternal extends iYesNo  {
  informationOnly: boolean
}

@Component({
  selector: 'core-dialog-yes-no',
  templateUrl: './core-dialog-yes-no.component.html',
  styleUrls: ['./core-dialog-yes-no.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FofCoreDialogYesNoComponent implements OnInit {
  // All input var
  @Input() anInput: any = undefined
  // All output notification
  @Output() anOuputEvent = new EventEmitter<any>()

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: iInternal,
    public matDialogRef: MatDialogRef<FofCoreDialogYesNoComponent>
    // private readonly coreAlertService: CoreAlertService,
    // private readonly coreErrorService: CoreErrorService
  ) {
    // this.matDialogRef.updatePosition({ top: '20px', left: '50px' });
    this.uiVar.question = data.question 

    if (data.title) {
      this.uiVar.title = data.title
    }    
    if (data.yesLabel) {
      this.uiVar.yesLabel = data.yesLabel
    }    
    if (data.noLabel) {
      this.uiVar.noLabel = data.noLabel
    }    
    if (data.informationOnly) {
      this.uiVar.informationOnly = data.informationOnly
    }
  }

  // All variables private to the class
  private priVar = {
  }
  // All private functions
  private privFunc = {
  }
  // All variables shared with UI HTML interface
  public uiVar = {
    title: 'fof has a question',
    question: '',
    yesLabel: 'yes',
    noLabel: 'no',
    informationOnly: false
  }
  // All actions accessible by user (could be used internaly too)
  public uiAction = {
  }
  // Angular events
  ngOnInit() {
   
  }
  ngAfterViewInit() {
    
  }
}
