import { Injectable, Injector } from '@angular/core'
import { NavigationStart, Router } from '@angular/router'
import { BehaviorSubject, Observable, Subject } from 'rxjs'
import { Notification, NotificationParams, NotificationType } from './notification-type'

@Injectable({
  providedIn: 'root'
})
export class FofNotificationService {
  public saveIsDoneNotification = new BehaviorSubject<any>(undefined)

  private subject = new Subject<Notification>()
  private keepAfterRouteChange = false

  constructor(
    // private router: Router
    // private injector: Injector    
  ) {
    // const router = this.injector.get(Router)
    // clear notification messages on route change unless 'keepAfterRouteChange' flag is true
    // router.events.subscribe(event => {
    //   if (event instanceof NavigationStart) {
    //     if (this.keepAfterRouteChange) {
    //       // only keep for a single route change
    //       this.keepAfterRouteChange = false
    //     } else {
    //       // clear notification messages
    //       this.clear()
    //     }
    //   }
    // })
  }

  get savedNotification () {
    return this.saveIsDoneNotification.asObservable()
  }

  getnotification(): Observable<any> {
    return this.subject.asObservable()
  }

  /** Notifiy a success 
   * @param message
   * accept html
   */
  success(message: string, params?: NotificationParams) {
    this.notification(NotificationType.Success, message, params)
  }

  /** Notifiy an error 
   * @param message
   * accept html
   */
  error(message: string, params?: NotificationParams) {
    this.notification(NotificationType.Error, message, params)
  }

  /** Notifiy an info
   * @param message
   * accept html
   */
  info(message: string, params?: NotificationParams) {
    this.notification(NotificationType.Info, message, params)
  }

  /** Notifiy a warning
   * @param message
   * accept html
   */
  warn(message: string, params?: NotificationParams) {
    this.notification(NotificationType.Warning, message, params)
  }


  notification(type: NotificationType, message: string, params?: NotificationParams) {
    const notification = new Notification()

    if (params) {
      if (params.keepAfterRouteChange) {
        this.keepAfterRouteChange = params.keepAfterRouteChange
      }
      if (params.mustDisappearAfter) {
        if (params.mustDisappearAfter < 0) {
          params.mustDisappearAfter = undefined
        } else {
          notification.mustDisappearAfter = params.mustDisappearAfter
        }        
      } else {
        notification.mustDisappearAfter = 3000
      }
    } else {
      notification.mustDisappearAfter = 3000
    }

    notification.type = type
    notification.message = message

    this.subject.next(notification)
  }

  saveIsDone() {
    this.saveIsDoneNotification.next(true)
  }

  clear() {
    // clear notifications
    this.subject.next()
  }
}
