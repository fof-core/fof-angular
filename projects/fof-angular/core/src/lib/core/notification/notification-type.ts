export class Notification {
  type: NotificationType = NotificationType.Info
  message: string = ''
  mustDisappearAfter?: number
}

export enum NotificationType {
  Success,
  Error,
  Info,
  Warning
}

/** params for notification */
export class NotificationParams {
  /** Keep the notification active, even if the route change */
  keepAfterRouteChange?: boolean
  /** Number in millisecond. By default, 3000. For preventing the notification to 
   * diseaper, set it with a negative number */
  mustDisappearAfter?: number
}