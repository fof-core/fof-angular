// Angular
import {Component, OnInit, OnDestroy, NgZone} from '@angular/core'
import {animate, state, style, transition, trigger} from '@angular/animations'
// Services
import { FofNotificationService } from '../notification.service'
// Models
import { Notification, NotificationType } from '../notification-type'
// Components
// Vendors & utils
import { Subscription } from 'rxjs'

// how to
// https://angular.io/guide/animations
// https://github.com/PointInside/ng2-toastr/blob/master/src/toast-container.component.ts

@Component({
  selector: 'fof-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  animations: [
    trigger('inOut', [     
      transition(':enter', [
        style({
          opacity: 0,
          transform: 'translateX(100%)'
        }),
        animate('0.2s ease-in')
      ]),
      transition(':leave', [
        animate('0.2s 10ms ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ])    
  ]
})
export class notificationComponent implements OnInit, OnDestroy {
  constructor(
    private fofNotificationService: FofNotificationService,
    private ngZone: NgZone
  ) { }

  // All private variables
  private priVar = {
    getnotificationSub: <Subscription><unknown>undefined,
    savedNotificationSub: <Subscription><unknown>undefined
  }
  // All private functions
  private privFunc = {
  }
  // All variables shared with UI 
  public uiVar = {
    notifications:<Array<Notification>>[],
    savedActive: false
  }
  // All actions shared with UI 
  public uiAction = {
    removenotification:(notification: Notification) => {
      this.uiVar.notifications = this.uiVar.notifications.filter(x => x !== notification)
    },
    cssClass:(notification: Notification) => {
      if (!notification) {
          return
      }

      // return css class based on notification type
      switch (notification.type) {
        case NotificationType.Success:
            return 'notification notification-success'
        case NotificationType.Error:
            return 'notification notification-danger'
        case NotificationType.Info:
            return 'notification notification-info'
        case NotificationType.Warning:
            return 'notification notification-warning'
      }
    }
  }
  // Angular events
  ngOnInit() {
    const template = this

    this.priVar.getnotificationSub = this.fofNotificationService.getnotification()
    .subscribe((notification: Notification) => {      
      if (!notification) {
          // clear notifications when an empty notification is received
          this.uiVar.notifications = []
          return
      }

      if (notification.mustDisappearAfter) {
        setTimeout(() => {
          template.uiAction.removenotification(notification)
        }, notification.mustDisappearAfter)
      }
      
      // ensure the notif will be displayed even if received from a promise pipe
      this.ngZone.run(() => {
        // push or unshift depend if there are on the top or bottom
        // this.notifications.push(notification)
        this.uiVar.notifications.unshift(notification)
      })
    })

    this.fofNotificationService.savedNotification
    .subscribe((saved) => {          
      if (!saved) { return }
      this.uiVar.savedActive = true
      setTimeout(() => {
        this.uiVar.savedActive = false
      }, 300)
    })
  }
  ngOnDestroy() {
    if (this.priVar.getnotificationSub) { this.priVar.getnotificationSub.unsubscribe() }
    if (this.priVar.savedNotificationSub) { this.priVar.savedNotificationSub.unsubscribe() }
  }
}
