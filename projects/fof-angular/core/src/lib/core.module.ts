import { ModuleWithProviders, NgModule } from '@angular/core'
import { AuthModule } from './auth/auth.module'
import { CORE_CONFIG, IFofConfig } from './fof-config'
import { FofLocalstorageService } from './fof-localstorage.service'
// import { PermissionsModule } from './permissions/permissions.module'
import { CoreModule } from './core/core.module'
import { ComponentsModule } from './components/components.module'

@NgModule({
  declarations: [

  ],
  imports: [
    CoreModule,
    AuthModule,
    // PermissionsModule
    ComponentsModule
  ],
  providers: [
    FofLocalstorageService
  ],
  exports: [
    CoreModule,
    ComponentsModule
  ]
})
export class FofCoreModule { 
  static forRoot(fofConfig: IFofConfig): ModuleWithProviders<FofCoreModule> {
    return {
      ngModule: FofCoreModule,      
      providers: [        
        {
          provide: CORE_CONFIG,
          useValue: fofConfig
        }
      ]
    }
  }
}
