import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FofUserRolesSelectComponent } from './fof-user-roles-select.component';

describe('FofUserRolesSelectComponent', () => {
  let component: FofUserRolesSelectComponent;
  let fixture: ComponentFixture<FofUserRolesSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FofUserRolesSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FofUserRolesSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
