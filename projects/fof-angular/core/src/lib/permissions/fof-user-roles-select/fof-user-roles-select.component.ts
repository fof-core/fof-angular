import { Component, OnInit, Input, OnChanges, Inject } from '@angular/core'
import { iRole, iUserRoleOrganization, iOrganization } from '../permissions.interface'
import { PermissionsService } from '../permissions.service'
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog'
import { FofNotificationService } from '../../core/notification/notification.service'
import { Observable } from 'rxjs'
import { FofErrorService } from '../../core/fof-error.service'
import { BaseService } from '../../core/base/base.service'
import { BaseComponent } from '../../core/base/component.base'
import { FofGenericService } from '../../components/generic.service'


interface iRoleUI extends iRole {
  checked?: boolean
  userRoleOrganization?: iUserRoleOrganization
}

@Component({
  selector: 'fof-user-roles-select',
  templateUrl: './fof-user-roles-select.component.html',
  styleUrls: ['./fof-user-roles-select.component.scss']
})
export class FofUserRolesSelectComponent extends BaseComponent implements OnInit {
  @Input() usersRolesOrganizations!: iUserRoleOrganization[] 
  @Input() notSelectableOrganizations!: iOrganization[]

  constructor (
    private matDialogRef:MatDialogRef<FofUserRolesSelectComponent>,
    private fofPermissionService: PermissionsService,
    private fofNotificationService: FofNotificationService,
    private fofErrorService: FofErrorService,
    @Inject(MAT_DIALOG_DATA) data: any,
    private baseService: BaseService,
    private fofGenericService: FofGenericService
  ) {

    super(baseService, {
      permissions: {
        // create: eCp.userRoleCreate,
        // delete: eCp.userRoleDelete,
        // update: eCp.userRoleUpdate
      }
    })
    // data: {
    //   roles: iRole[],
    //   usersRolesOrganizations: iUserRoleOrganization[],
    //   userId: number,
    //   notSelectableOrganizations: iOrganization[],
    //   serviceBackUrl: string
    // }

    console.log('data', data)

    this.usersRolesOrganizations = data.usersRolesOrganizations
    this.uiVar.allRoles = data.roles.data
    this.priVar.userId = data.userId
    this.priVar.serviceBackUrl = data.serviceBackUrl

    if (data.notSelectableOrganizations) {
      this.notSelectableOrganizations = data.notSelectableOrganizations 
    }

    if (this.uiVar.allRoles && this.uiVar.allRoles.length) {
      this.uiVar.allRoles.forEach((role: iRoleUI) => {
        role.checked = false
      })
    }

    this.privFunc.componenentDataRefresh()
  }

  // All private variables
  private priVar = {
    userId: <string><unknown>undefined,
    organizationId: <string><unknown>undefined,
    serviceBackUrl: <string><unknown>undefined
  }
  // All private functions
  private privFunc = {
    componenentDataRefresh:() => {

      if (this.usersRolesOrganizations) {
        // it's an update
        this.uiVar.isCreation = false
        this.uiVar.title = `Access update`
        // Only one organization possible in update
        this.uiVar.organizationMultipleSelect = false
        if (this.usersRolesOrganizations.length > 0) {
          // we get only the first one since it's impossible to have mutiple organisations
          this.priVar.organizationId = this.usersRolesOrganizations[0].organization?.id || ''
          this.uiVar.selectedOrganisations = []
          if (this.usersRolesOrganizations[0].organization) {
            this.uiVar.selectedOrganisations.push(
              this.usersRolesOrganizations[0].organization
            )
          }          
        }
  
        if (this.uiVar.allRoles) {
          this.uiVar.allRoles.forEach((role: iRoleUI) => {
            const userRoleOrganization = this.usersRolesOrganizations.find(item => item.roleId == role.id)
            if (userRoleOrganization) {
              role.userRoleOrganization = userRoleOrganization
              role.checked = true
            }
          })
        }     
      } else {
        // it's a creation
        this.uiVar.isCreation = true
        this.uiVar.title = `New access`
      }
     
    }
  }
  // All variables shared with UI 
  public uiVar = {
    allRoles: <any[]><unknown>undefined,
    organizationMultipleSelect: <boolean>true,
    selectedOrganisations: <iOrganization[]>[],
    title: <string>`New access`,
    isCreation: <boolean>false,
    loading: false
  }
  // All actions shared with UI 
  public uiAction = {
    organisationMultiSelectedChange:(organisations: iOrganization[]) => {      
      this.uiVar.selectedOrganisations = organisations      
    },
    userRoleOrganizationSave:(role: iRoleUI) => {      
      // const userRoleOrganization:iUserRoleOrganization = role.userRoleOrganization
    },
    save:() => {
      console.log('save')
      const usersRolesOrganizations: iUserRoleOrganization[] = []
      let userRoleOrganization: iUserRoleOrganization

      if (!this.priVar.userId) {
        this.fofNotificationService.error(`
          Data error<br>
          <small>User is missing</small>
        `)
      }

      this.uiVar.selectedOrganisations.forEach(organisation => {
        this.uiVar.allRoles.forEach((role: iRoleUI) => {
          if (role.checked) {
            userRoleOrganization = {
              userId: this.priVar.userId,
              organizationId: organisation.id,
              roleId: role.id || ''
            }
            usersRolesOrganizations.push(userRoleOrganization)
          }
        })
      })

      if (usersRolesOrganizations.length === 0) {
        this.fofNotificationService.error(`
          You must select, at least<br>
          <ul>
            <li>One organization</li>
            <li>One role</li>
          </ul>
        `, {mustDisappearAfter: -1})

        return
      }

      this.uiVar.loading = true
      
      if (this.uiVar.isCreation) { 
        
        this.fofPermissionService.role.createLinkWithUserAndOrganization(usersRolesOrganizations)
          .toPromise()
          .then(result => {
            this.matDialogRef.close({saved: true})          
          })
          .catch(reason => {
            this.fofErrorService.errorManage(reason)         
          }) 
          .finally(() => {
            this.uiVar.loading = false
          })
        
      } else {                
        this.fofPermissionService.user        
        .replaceUserRoleOrganization(usersRolesOrganizations, this.priVar.userId, this.priVar.organizationId, this.priVar.serviceBackUrl)
        .toPromise()
        .then(result => {
          this.matDialogRef.close({saved: true})          
        })
        .catch(reason => {
          this.fofErrorService.errorManage(reason)
        })
        .finally(() => {
          this.uiVar.loading = false
        })
      }

      
      
    }
  }
  // Angular events
  ngOnInit() {
    
  } 
  ngOnChanges() {
    this.privFunc.componenentDataRefresh()    
  } 
}
