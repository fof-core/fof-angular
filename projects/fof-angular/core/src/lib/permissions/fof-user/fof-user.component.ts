import { Component, OnInit } from '@angular/core'
import { PermissionsService } from '../permissions.service'
import { Router, ActivatedRoute } from '@angular/router'
import { iUser, iOrganization, iUserLogin } from '../permissions.interface'
import { FormBuilder, Validators  } from "@angular/forms"
import { FofNotificationService } from '../../core/notification/notification.service'
import { FofDialogService } from '../../core/fof-dialog.service'
import { fofUtilsForm } from '../../core/fof-utils'
import { FofErrorService } from '../../core/fof-error.service'
import { eCp } from '../permission.enum'
// import { FoFAuthService } from '../../core/auth.service'
import { takeUntil } from 'rxjs/operators'
import { Subject } from 'rxjs'
import { BaseService } from '../../core/base/base.service'
import { BaseComponent } from '../../core/base/component.base'
import { FofGenericService } from '../../components/generic.service'

@Component({
  selector: 'fof-core-fof-user',
  templateUrl: './fof-user.component.html',
  styleUrls: ['./fof-user.component.scss']
})
export class FofUserComponent extends BaseComponent implements OnInit {
  constructor(
    private permissionsService: PermissionsService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private fofNotificationService: FofNotificationService,
    private fofDialogService: FofDialogService,
    private router: Router,    
    private fofErrorService: FofErrorService,
    private baseService: BaseService,
    private fofGenericService: FofGenericService
  ) {  
    super(baseService, {
      permissions: {
        create: eCp.userCreate,
        delete: eCp.userDelete,
        update: eCp.userUpdate
      }
    })         
  }

  // All private variables
  private priVar = {    
    userOrganizationId: <string | undefined>undefined,
    organizationAlreadyWithRoles: <iOrganization[]><unknown>undefined,
    destroy$: new Subject<void>()
  }
  // All private functions
  private privFunc = {
    userLoad:() => {

      if (!this.uiVar.userId) {
        this.fofNotificationService.error(`This user doesn't exist`)
        this.router.navigate(['../'], {relativeTo: this.activatedRoute})
        return
      }

      this.uiVar.loadingUser = true      
      Promise.all([        
        this.permissionsService.user.getWithRoleById(this.uiVar.userId).toPromise()        
      ])
      .then(result => {        
        const currentUsers = result[0]
     
        this.privFunc.userRefresh(currentUsers)          
        
        this.uiVar.form.patchValue(this.uiVar.user)                
      })
      .catch(reason => {        
        this.fofErrorService.errorManage(reason)
        this.router.navigate(['../'], {relativeTo: this.activatedRoute})
      })
      .finally(() => {
        this.uiVar.loadingUser = false
      })
    },
    userRefresh:(currentUser: iUser) => {
      
      if (!currentUser) {
        this.fofNotificationService.error(`Ce utilisateur n'existe pas`)
        this.router.navigate(['../'], {relativeTo: this.activatedRoute})
        return
      }

      this.uiVar.user = currentUser      

      if (currentUser.organizationId) {
        this.uiVar.userOrganizationsDisplay = [{id: currentUser.organizationId}]
        this.priVar.userOrganizationId = currentUser.organizationId
      }

    }
  }
  // All variables shared with UI 
  public uiVar = {
    title: 'New user',    
    loadingUser: false,    
    userIsNew: false,
    userId: <string | undefined>undefined,
    user: <iUser><unknown>undefined,
    userOrganizationsDisplay: <iOrganization[]><unknown>undefined,
    form: this.formBuilder.group({      
      // login: ['', [Validators.maxLength(30)]],
      eMail: ['', [Validators.required, Validators.email, Validators.maxLength(60)]],
      firstName: ['', [Validators.maxLength(30)]],
      lastName: ['', [Validators.maxLength(30)]],
      isActive: [true, []]
    })
  }
  // All actions shared with UI 
  public uiAction = {
    userSave:() => {      
      const userToSave: iUser = this.uiVar.form.value
      userToSave.organizationId = this.priVar.userOrganizationId

      if (!userToSave.organizationId) {
        this.fofNotificationService.error('You must set an organization')
        return 
      }

      if (!this.uiVar.form.valid) {
        this.fofNotificationService.error('Please, corerect mistakes before saving')
        fofUtilsForm.validateAllFields(this.uiVar.form)
        return
      }

      if (this.uiVar.userIsNew) {        
        this.fofGenericService.endpoints.create('users', userToSave)
        .toPromise()
        .then((newUser: iUser) => {
          this.fofNotificationService.success('User saved', {mustDisappearAfter: 1000})
          this.uiVar.userId = newUser.id 
          this.uiVar.title = `User modification`
          this.uiVar.userIsNew = false
          this.privFunc.userLoad()          
        })
        .catch((reason: any) => {
          this.fofErrorService.errorManage(reason)
        })
      } else {
        userToSave.id = this.uiVar.user.id 
      
        this.fofGenericService.endpoints.create('users', userToSave)
        .toPromise()
        .then((result: any) => {
          this.fofNotificationService.success('User saved', {mustDisappearAfter: 1000})
        }) 
        .catch((reason: any) => {
          this.fofErrorService.errorManage(reason)
        })
      }
    },
    userCancel:() => {      
      this.privFunc.userLoad()
    },
    userDelete:() => {      
      this.fofDialogService.openYesNo({   
        title: 'Delete user',     
        question: `Really want to delete the user?`
      }).then(yes => {
        if (yes) {          
          this.fofGenericService.endpoints.delete('users', this.uiVar.user)
          .toPromise()
          .then((result: any) => {
            this.fofNotificationService.success('Utilisateur supprimé')
            this.router.navigate(['../'], {relativeTo: this.activatedRoute})
          })
          .catch((reason: any) => {
            this.fofErrorService.errorManage(reason)
          })
        }
      })
    },
    organisationMultiSelectedChange:(organizations: iOrganization[]) => {      
      if (organizations && organizations.length > 0) {
        this.priVar.userOrganizationId = organizations[0].id 
      } else {
        (this.priVar.userOrganizationId as any) = null
      }
    }
  }
  // Angular events
  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      const code:any = params.get('code')      
      if (code) {
        if (code.toLowerCase() == 'new') {
          this.uiVar.userIsNew = true
        } else {
          this.uiVar.userId = code
          this.uiVar.title = `User modification`
          this.privFunc.userLoad()
        }
      }      
    })
  }
  ngOnDestroy() {
    this.priVar.destroy$.next()    
  }
}
