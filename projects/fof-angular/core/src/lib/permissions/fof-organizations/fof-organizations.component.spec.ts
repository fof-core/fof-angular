import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FofOrganizationsComponent } from './fof-organizations.component';

describe('FofOrganizationsComponent', () => {
  let component: FofOrganizationsComponent;
  let fixture: ComponentFixture<FofOrganizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FofOrganizationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FofOrganizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
