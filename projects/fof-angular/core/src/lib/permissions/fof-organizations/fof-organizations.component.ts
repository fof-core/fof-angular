import { Component, OnInit, ɵNG_INJ_DEF, ViewChild } from '@angular/core'
import { PermissionsService } from '../permissions.service'
import { UserService } from '../../auth/user.service'
import { entUser } from '../../auth/user.entity'
import { NestedTreeControl } from '@angular/cdk/tree'
import { MatTreeNestedDataSource } from '@angular/material/tree'
import { iOrganization, iUserLogin } from '../permissions.interface'
import { FormBuilder, Validators  } from "@angular/forms"
import { FofNotificationService } from '../../core/notification/notification.service'
import { FofDialogService } from '../../core/fof-dialog.service'
import { fofUtilsForm } from '../../core/fof-utils'
import { FofErrorService } from '../../core/fof-error.service'
import { eCp } from '../permission.enum'
// import { FoFAuthService } from '../../core/auth.service'
import { takeUntil } from 'rxjs/operators'
import { Subject } from 'rxjs'
import { FofOrganizationsTreeComponent } from '../fof-organizations-tree/fof-organizations-tree.component'
import { BaseComponent } from '../../core/base/component.base'
import { BaseService } from '../../core/base/base.service'

interface iOrgUI extends iOrganization {
  checked?: boolean,
  indeterminate?: boolean,
  children?: iOrgUI[],
  isRoot?: boolean
}

@Component({
  selector: 'fof-core-fof-organizations',
  templateUrl: './fof-organizations.component.html',
  styleUrls: ['./fof-organizations.component.scss']
})
export class FofOrganizationsComponent extends BaseComponent implements OnInit {
  @ViewChild(FofOrganizationsTreeComponent) private cOrganizationTree!: FofOrganizationsTreeComponent
 
  constructor(
    private permissionService: PermissionsService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private fofNotificationService: FofNotificationService,
    private fofDialogService: FofDialogService,
    private fofErrorService: FofErrorService,
    // private foFAuthService: FoFAuthService
    private baseService: BaseService
  ) { 
    super(
      baseService,
      {
        permissions: {
          create: eCp.organizationCreate,
          update: eCp.organizationUpdate,
          delete: eCp.organizationDelete
      }}
    )    
      
  }

  // All private variables
  private priVar = {    
    parentIdChange: <number><unknown>undefined   
  }
  // All private functions
  private privFunc = {
  }
  // All variables shared with UI 
  public uiVar = {
    currentNode: <iOrgUI><unknown>undefined,
    loading: false,
    organizationIsNew: false,  
    nodeForm: false,
    form: this.formBuilder.group({ 
      uid: ['', []],
      name: ['', [Validators.required, Validators.maxLength(100)]],
      description: ['', [Validators.maxLength(200)]],
    }),
    actionTitle: `Selected organization`,    
    nodeChanged!: <iOrganization><unknown>null,
    nodeToDelete: <iOrganization><unknown>null,    
    organizationFatherDisplay: <iOrganization[]><unknown>null
  }
  // All actions shared with UI 
  public uiAction = {    
    organisationAdd:() => {
      if (!this.uiVar.currentNode) {
        return
      }
      this.uiVar.form.reset()
      this.uiVar.organizationFatherDisplay = [{id: this.uiVar.currentNode.id}]
      this.uiVar.organizationIsNew = true
      this.uiVar.actionTitle = `Add a child`
    },
    organisationDelete:() => {

      const nodeToDelete = this.uiVar.currentNode
     
      this.fofDialogService.openYesNo({
        title: "Supprimer une organisation",
        question: `Voulez vous vraiment supprimer 
          ${nodeToDelete.name} ?`
      }).then(yes => {        
        if (yes) {          
          this.permissionService.organization.delete(nodeToDelete)
          .toPromise()
          .then(result => {
            this.uiVar.nodeToDelete = nodeToDelete
            this.uiAction.organisationCancel()
          })
          .catch(reason => {            
            if (reason.isConstraintError) {
              this.fofNotificationService.info(`Vous ne pouvez pas supprimer l'organisation !<br>
                <small>Vous devez supprimer tous les objets rattachés à l'organisation ou contacter un admninistrateur</small>`
                , {mustDisappearAfter: -1})
            } else {
              this.fofErrorService.errorManage(reason)
            }            
          })
        }
      })
      this.uiVar.organizationIsNew = false
    },
    organisationSave:() => {
      const nodeToSave: iOrganization = this.uiVar.form.value     

      if (!this.uiVar.form.valid) {
        this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder')
        fofUtilsForm.validateAllFields(this.uiVar.form)
        return
      }

      // New organization
      if (this.uiVar.organizationIsNew) {
        // nodeToSave.parentId = this.uiVar.currentNode.id        
        this.permissionService.organization.create({      
          parentId: this.uiVar.currentNode.id,     
          uid: nodeToSave.uid,
          name: nodeToSave.name
        })
        .toPromise()
        .then((nodeResult: iOrganization) => {          
          this.fofNotificationService.saveIsDone()
          if (this.uiVar.currentNode.children) {
            this.uiVar.currentNode.children.push(nodeResult)  
          }
          

          // deep copy for pushing the tree component to refresh the object
          this.uiVar.nodeChanged = JSON.parse(JSON.stringify(this.uiVar.currentNode))
          
          this.uiAction.selectedOrganizationsChange(this.uiVar.currentNode)
          this.uiVar.organizationIsNew = false
          // setTimeout(() => {
          //   this.cOrganizationTree.uiAction.organizationTreeLoad()
          // }, 100)
        })
        .catch(reason => {
          this.fofErrorService.errorManage(reason)
        })      
      } else {
        // Update an organization
        this.uiVar.currentNode.name = nodeToSave.name
        
        this.permissionService.organization.update({
          id: this.uiVar.currentNode.id,
          uid: this.uiVar.currentNode.uid,
          name: this.uiVar.currentNode.name,
          parentId: this.uiVar.currentNode.parentId
        })
        .toPromise()
        .then((nodeResult: iOrganization) => {        
          this.uiVar.nodeChanged = nodeResult         
          this.fofNotificationService.saveIsDone()          
        })
        .catch(reason => {
          this.fofErrorService.errorManage(reason)
        })
      }      
    },
    organisationCancel:() => {            
      this.uiVar.actionTitle = `Selected organization`
      this.uiVar.nodeForm = false
      this.uiVar.organizationIsNew = false
    },
    selectedOrganizationsChange:(node: iOrgUI | any) => {     
      this.uiAction.organisationCancel()

      if (node) {
        this.uiVar.nodeForm = node.checked || false
        if (node.parentId) {          
          this.uiVar.organizationFatherDisplay = [{id: node.parentId}]    
        } else {
          (this.uiVar.organizationFatherDisplay as any) = null
        }
        this.uiVar.form.patchValue(node)
      } else {
        this.uiVar.nodeForm = false
        this.uiVar.form.reset()
      }

     
 
      if (node && node.checked) {
        if (node.uid == 'root') {
          node.isRoot = true
        }
        this.uiVar.currentNode = node        
      } else {
        (this.uiVar.currentNode as any) = null        
      }
    },
    organisationMultiSelectedChange:(node: iOrgUI[] | any) => {            
      if (node && node.length) {        
        this.uiVar.currentNode.parentId = node[0].id
      }      
    }
  }
  // Angular events
  ngOnInit() {
  
  }  

  ngOnDestroy() {   
    this.destroy()     
  }
}
