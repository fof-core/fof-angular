import { Component, OnInit, Input } from '@angular/core'
import { PermissionsService } from '../permissions.service'
import { Router, ActivatedRoute } from '@angular/router'
import { iRole, iUser, iUserRoleOrganization, iOrganization, iUserLogin } from '../permissions.interface'
import { FofNotificationService } from '../../core/notification/notification.service'
import { FofDialogService } from '../../core/fof-dialog.service'
import { MatDialog } from '@angular/material/dialog'
import { FofUserRolesSelectComponent } from '../fof-user-roles-select/fof-user-roles-select.component'
import { FofErrorService } from '../../core/fof-error.service'
import { eCp } from '../permission.enum'
// import { FoFAuthService } from '../../core/auth.service'
import { takeUntil } from 'rxjs/operators'
import { Subject } from 'rxjs'
import { BaseService } from '../../core/base/base.service'
import { BaseComponent } from '../../core/base/component.base'

interface iRoleUI extends iRole {
  checked?: boolean
  userRoleOrganization?: iUserRoleOrganization
}

interface iUserRoleToDisplay {
  organizationId?: string,
  organization?: string,
  roles?: string[]
  usersRolesOrganizations?: iUserRoleOrganization[],
  checked?: boolean
}

@Component({
  selector: 'fof-user-roles',
  templateUrl: './fof-user-roles.component.html',
  styleUrls: ['./fof-user-roles.component.scss']
})
export class FofUserRolesComponent extends BaseComponent implements OnInit {  
  @Input() userId?: string  
  @Input() serviceBackUrl!: string
  @Input() noExpension: boolean = false

  constructor(
    private fofPermissionService: PermissionsService,
    private activatedRoute: ActivatedRoute,    
    private fofNotificationService: FofNotificationService,
    private fofDialogService: FofDialogService,
    private router: Router,
    private matDialog: MatDialog,
    private fofErrorService: FofErrorService,
    private baseService: BaseService    
  ) { 
    super(baseService, {
      permissions: {
        create: eCp.userRoleCreate,
        delete: eCp.userRoleDelete,
        update: eCp.userRoleUpdate
      }
    })

    this.uiVar.roleDisplayedColumns = ['delete', 'organization', 'roles']

    // this.foFAuthService.currentUser$
    // .pipe(takeUntil(this.priVar.destroy$))
    // .subscribe((user: iUserLogin) => {  
    //   if (user.permissions.includes(eCp.userRoleCreate)) {
    //     this.uiVar.permissions.userRoleCreate = true
    //   }     
    //   if (user.permissions.includes(eCp.userRoleDelete)) {
    //     this.uiVar.permissions.userRoleDelete = true
    //   }   
    //   if (user.permissions.includes(eCp.userRoleUpdate)) {
    //     this.uiVar.permissions.userRoleUpdate = true
    //     this.uiVar.roleDisplayedColumns.push('icon')
    //   }         
    // }) 
  }

  // All private variables
  private priVar = {    
    organizationAlreadyWithRoles: <iOrganization[]><unknown>undefined,
    destroy$: new Subject<void>()
  }
  // All private functions
  private privFunc = {
    userLoad:() => {
      // this.uiVar.loadingUser = true
      if (!this.userId) {
        return
      }

      Promise.all([
        this.fofPermissionService.role.getAll(this.serviceBackUrl).toPromise(),
        this.fofPermissionService.user.getWithRoleById(this.userId, this.serviceBackUrl).toPromise()        
      ])
      .then(result => {
        // console.log('result', result)
        const roles: iRole[] = result[0]                    
        const currentUsers = result[1]

        this.uiVar.allRoles = roles
        this.privFunc.userRefresh(currentUsers)          
        // Here for preventing to refresh the user form when refreshing only the roles.
        // toDo: improve
                       
      })
      .catch(reason => {        
        this.fofErrorService.errorManage(reason)
        // this.router.navigate(['../'], {relativeTo: this.activatedRoute})
      })
      .finally(() => {
        // this.uiVar.loadingUser = false
      })
    },  
    userRefresh:(currentUser: iUser) => {
      
      if (!currentUser) {
        this.fofNotificationService.error(`Ce utilisateur n'existe pas`)
        // this.router.navigate(['../'], {relativeTo: this.activatedRoute})
        return
      }

      this.uiVar.user = currentUser      

      let _previousOrganisationId!: string 
      let _roleToDisplay!: iUserRoleToDisplay 
      
      this.uiVar.userRolesToDisplay = []

      if (currentUser.usersRolesOrganizations && currentUser.usersRolesOrganizations.length > 0) {                
        // we will display the user roles grouped by organization 
        // with one on several roles on it
        currentUser.usersRolesOrganizations.forEach(uro => {
          if (_previousOrganisationId !== uro.organization?.id) {
            // it's a new organisation, reinit
            _previousOrganisationId = uro.organization?.id || ''
            if (_roleToDisplay) { this.uiVar.userRolesToDisplay.push(_roleToDisplay) }
            _roleToDisplay = { 
              organizationId: uro.organization?.id,
              organization: uro.organization?.name,
              roles: [],
              usersRolesOrganizations: []
            }            
          }
          
          _roleToDisplay.roles?.push(uro?.role?.code || '')          
          _roleToDisplay.usersRolesOrganizations?.push(uro)
        })            
      }

      // add the last one
      if (_roleToDisplay) { this.uiVar.userRolesToDisplay.push(_roleToDisplay) }

      // we don't want the following organization could be selectebale in the 
      // organization tree
      this.priVar.organizationAlreadyWithRoles = []
      this.uiVar.userRolesToDisplay.forEach(org => {
        this.priVar.organizationAlreadyWithRoles.push({
          id: org.organizationId,
          name: org.organization
        })
      })

    },
    useRolesRefresh:() => {      
      this.uiVar.loadingRoles = true
      this.fofPermissionService.user.getWithRoleById(this.userId || '', this.serviceBackUrl)
      .toPromise()
      .then(usersResult => {
        this.privFunc.userRefresh(usersResult)            
      })
      .catch(reason => {
        this.fofErrorService.errorManage(reason)
        // this.router.navigate(['../'], {relativeTo: this.activatedRoute})
      })
      .finally(() => {
        this.uiVar.loadingRoles = false
      })       
    }
  }
  // All variables shared with UI 
  public uiVar = { 
    user: <iUser><unknown>undefined,     
    loadingRoles: false,    
    allRoles: <iRoleUI[]><unknown>undefined,
    selectedUserRoleOrganizations: <iUserRoleOrganization[]><unknown>undefined,
    userRolesToDisplay: <iUserRoleToDisplay[]>[],
    roleDisplayedColumns: <string[]>[]     
  }
  // All actions shared with UI 
  public uiAction = {         
    userRolesDelete:() => {
      const userRoles = this.uiVar.userRolesToDisplay
      const organizationsIdToDelete: Array<string> = []
      let somethingToDelete = false
      let message = `Voulez vous vraiment supprimer les rôles des organisations suivante ?<br>`
      message = message + '<ul>'

      userRoles.forEach(ur => {        
        if (ur['checked']) {
          somethingToDelete = true
          message = message + `<li>${ur.organization}</li>`
          if (ur.organizationId) {
            organizationsIdToDelete.push(ur.organizationId)
          }          
        }      
      })

      message = message + '</ul>'

      if (somethingToDelete) {
        this.fofDialogService.openYesNo({        
          title: 'Supprimer rôles',
          question: message
        }).then(yes => {
          if (yes) {            
            if (this.uiVar.user.id) {
              this.fofPermissionService.user.deleteUserRoleOrganizations(this.uiVar.user.id, organizationsIdToDelete, this.serviceBackUrl)
                .toPromise()
                .then(result => {
                  this.fofNotificationService.success('Rôles supprimés')
                  this.privFunc.useRolesRefresh()              
                }) 
            }            
          }
        })
      } else {
        this.fofNotificationService.info('Vous devez sélectionner ua moins une organisation')
      }
    },
    roleSelectComponentOpen:(userRole?: iUserRoleToDisplay) => {   
      let usersRolesOrganizations!: iUserRoleOrganization[]

      if (userRole) {        
        if (!this.base.permissions.update) {
          return
        }
        if (userRole.usersRolesOrganizations) {
          usersRolesOrganizations = userRole.usersRolesOrganizations
        }        
      }

      const dialogRef = this.matDialog.open(FofUserRolesSelectComponent, {
        data: {
          roles: this.uiVar.allRoles,
          usersRolesOrganizations: usersRolesOrganizations,
          notSelectableOrganizations: this.priVar.organizationAlreadyWithRoles,
          userId: this.uiVar.user.id,
          serviceBackUrl: this.serviceBackUrl
        },
        width: '600px' ,
        // height: 'calc(100vh - 200px)'      
      })
      dialogRef.afterClosed()
      .toPromise()
      .then(mustBeRefresd => {        
        if (mustBeRefresd) {
          this.privFunc.useRolesRefresh()
        }        
      })
    }
  }
  // Angular events
  ngOnInit() {
    
  }
  ngOnChanges() {
    if (this.userId) {
      this.privFunc.userLoad()
    }
    // if (this.serviceBackUrl) {
    //   console.log('serviceBackUrl', this.serviceBackUrl)
    // }
  }

  ngOnDestroy() {
    this.priVar.destroy$.next()    
  }
}
