import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FofUserRolesComponent } from './fof-user-roles.component';

describe('FofUserRolesComponent', () => {
  let component: FofUserRolesComponent;
  let fixture: ComponentFixture<FofUserRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FofUserRolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FofUserRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
