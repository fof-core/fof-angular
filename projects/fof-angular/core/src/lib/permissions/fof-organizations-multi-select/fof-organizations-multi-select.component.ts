import { Component, OnInit, ElementRef, Output, EventEmitter, Input, OnChanges } from '@angular/core'
import { iOrganization } from '../permissions.interface'
import { PermissionsService, iOrgUi } from '../permissions.service'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'fof-core-fof-organizations-multi-select',
  templateUrl: './fof-organizations-multi-select.component.html',
  styleUrls: ['./fof-organizations-multi-select.component.scss'],
  host: {
    '(document:click)': 'onDocumentClick($event)',
  }
})
export class FofOrganizationsMultiSelectComponent implements OnInit {

  onDocumentClick(event: any) {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.uiVar.treeMenuVisible = false
    }
  }

  @Input() multiSelect: boolean = true
  @Input() selectedOrganisations: iOrganization[] = []
  @Input() notSelectableOrganizations!: iOrganization[]
  @Input() placeHolder: string = 'Organisations'
  @Output() selectedOrganizationsChange = new EventEmitter()
  
  constructor (
    private elementRef: ElementRef,
    private fofPermissionService: PermissionsService
  ) { 
    
  }

  // All private variables
  private priVar = { 
    destroy$: new Subject<void>(),
    organizationsReady: false
  }
  // All private functions
  private privFunc = {
    refreshOuputs:() => {
      this.selectedOrganizationsChange.emit(this.selectedOrganisations)
    }
  }
  // All variables shared with UI 
  public uiVar = {
    treeMenuVisible: false,    
    // selectedOrganisations: <iOrganization[]>[]
  }
  // All actions shared with UI 
  public uiAction = {
    openTree:() => {
      this.uiVar.treeMenuVisible = !this.uiVar.treeMenuVisible
    },
    selectedOrganizationsChange: (selectedOrganisations: iOrganization[] | iOrganization) => {      
      if (!selectedOrganisations) {
        (this.selectedOrganisations as any) = null
        return
      }

      if (Array.isArray(selectedOrganisations )) {
        this.selectedOrganisations = selectedOrganisations  
      } else {
        this.selectedOrganisations = [selectedOrganisations]
      }

      this.selectedOrganisations.forEach((organization: iOrgUi) => {
        const nodeFullPath = this.fofPermissionService.organization.node.getOneById(organization.id || '')
        organization.fullPath = nodeFullPath.fullPath            
      })
      
      this.privFunc.refreshOuputs()
    },   
    // Remove organisation from chip
    organisationRemove:(organisationToRemove: iOrganization) => {
      this.selectedOrganisations = this.selectedOrganisations.filter (
        organisation => organisationToRemove.id !== organisation.id )
      this.privFunc.refreshOuputs()
    }
  }
  // Angular events
  ngOnInit() {
    this.fofPermissionService.organization.treeViewReady$
    .pipe(takeUntil(this.priVar.destroy$))
    .subscribe(ready => {      
      if (ready) {
        this.priVar.organizationsReady = true
        if (this.selectedOrganisations) {
          this.selectedOrganisations.forEach((organization: iOrgUi) => {            
            const orgWIthPath = this.fofPermissionService.organization.node.getOneById(organization.id || '')            
            organization.fullPath = orgWIthPath.fullPath
          })
        }    
      }
    })
  }
  ngOnChanges() {
    if (this.selectedOrganisations && this.priVar.organizationsReady) {
      this.selectedOrganisations.forEach((organization: iOrgUi) => {
        const orgWIthPath = this.fofPermissionService.organization.node.getOneById(organization.id || '')
        organization.fullPath = orgWIthPath.fullPath            
      })      
    }   
  }
  ngOnDestroy() {
    this.priVar.destroy$.next()    
  }
}