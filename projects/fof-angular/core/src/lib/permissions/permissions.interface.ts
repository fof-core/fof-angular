import { iFofEntityCore } from './entityCore.interface'

export interface iPermission extends iFofEntityCore {
  code:	string,
  description?: string,
  rolePermissions?: iRolePermission[]
}

export interface iRole extends iFofEntityCore {    
  code: string,
  description_: string,
  userRoleOrganizations?: iUserRoleOrganization[],
  permissions?: iRolePermission[]
}

export interface iUser extends iFofEntityCore {
  email: string,
  login?: string,  
  firstName?: string,
  lastName?: string,
  usersRolesOrganizations?: iUserRoleOrganization[],
  // userMiniservices?: iUserMiniservice[]  
}

export interface iOrganization extends iFofEntityCore {
  uid?:	string
  name?:	string
  description?:string
  children?: iOrganization[]
  parentId?:	string
  parent?: iOrganization[]
  isSynchronized?: boolean,
  fullPath?: string
}

export interface iUserLogin extends iUser {
  windowAuthentification?: boolean,
  accessToken?: string,
  permissions?: Array<string>,
}

export interface iRolePermission extends iFofEntityCore {
  permissionId?: string,
  roleId?: string,
  order?: number,
  permission?: iPermission
  role?: iRole
}

export interface iUserRoleOrganization extends iFofEntityCore {  
  userId: string,
  roleId: string,
  organizationId?: string,
  order?: number,
  user?: iUser,
  role?: iRole
  organization?: iOrganization 
}

export interface iUservice extends iFofEntityCore {
  technicalName: string
  name?: string
  frontUrl?: string
  backUrl?: string
  availableForUsers?: boolean
}

export interface iUserMiniservice extends iFofEntityCore {
  userId?: string,
  miniServiceId?: number
}
