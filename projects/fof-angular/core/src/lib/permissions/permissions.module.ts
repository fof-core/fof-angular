import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { BrowserModule, Title } from '@angular/platform-browser'
import { PermissionsRoutingModule } from './permissions-routing.module'
import { PermissionsService } from './permissions.service'
import { FofOrganizationsTreeComponent } from './fof-organizations-tree/fof-organizations-tree.component'
import { MaterialModule } from '../core/material.module'
import { FofOrganizationsComponent } from './fof-organizations/fof-organizations.component'
import { FofRolesComponent } from './fof-roles/fof-roles.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { FofOrganizationsMultiSelectComponent } from './fof-organizations-multi-select/fof-organizations-multi-select.component'
import { ComponentsModule } from '../components/components.module'
import { FofRoleComponent } from './fof-role/fof-role.component'
import { FofUsersComponent } from './fof-users/fof-users.component'
import { FofUserComponent } from './fof-user/fof-user.component'
import { FofUserRolesSelectComponent } from './fof-user-roles-select/fof-user-roles-select.component'
import { FofUserRolesComponent } from './fof-user-roles/fof-user-roles.component'

@NgModule({
  declarations: [
    FofOrganizationsTreeComponent,
    FofOrganizationsComponent,
    FofOrganizationsMultiSelectComponent,
    FofRolesComponent,
    FofRoleComponent,
    FofUsersComponent,
    FofUserComponent,
    FofUserRolesSelectComponent,
    FofUserRolesComponent
  ],
  providers: [
    PermissionsService,
    Title
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule, 
    ReactiveFormsModule,
    PermissionsRoutingModule,
    ComponentsModule
  ]
})
export class PermissionsModule { }
