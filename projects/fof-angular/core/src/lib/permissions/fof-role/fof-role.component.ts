import { Component, OnInit } from '@angular/core'
import { FormBuilder, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { forkJoin, Subject } from 'rxjs'
import { FofDialogService } from '../../core/fof-dialog.service'
import { fofUtilsForm } from '../../core/fof-utils'
import { FofNotificationService } from '../../core/notification/notification.service'
import { iPermission, iRole, iRolePermission } from '../permissions.interface'
import { PermissionsService } from '../permissions.service'
import { FofGenericService } from '../../components/generic.service'
import { BaseService } from '../../core/base/base.service'
import { BaseComponent } from '../../core/base/component.base'
import { eCp } from '../permission.enum'
import { FofErrorService } from '../../core/fof-error.service'

@Component({
  selector: 'core-fof-role',
  templateUrl: './fof-role.component.html',
  styleUrls: ['./fof-role.component.scss']
})
export class FofRoleComponent extends BaseComponent implements OnInit {
  constructor(
    private permissionsService: PermissionsService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private fofNotificationService: FofNotificationService,
    private fofDialogService: FofDialogService,
    private router: Router,
    private fofGenericService: FofGenericService,
    private baseService: BaseService,
    // private foFAuthService: FoFAuthService
    private fofErrorService: FofErrorService
  ) { 
    super(baseService, {
      permissions: {
        create: eCp.roleCreate,
        delete: eCp.roleDelete,
        update: eCp.roleUpdate
      }
    })     
  }

  // All private variables
  private priVar = {
    id: <string><unknown>undefined,    
    permissionRoluUpdating: <boolean>false,
    destroy$: new Subject<void>()
  }
  // All private functions
  private privFunc = {
    roleLoad:() => {      
      Promise.all([
        this.fofGenericService.endpoints.getOneById('roles', this.priVar.id).toPromise(),
        this.fofGenericService.endpoints.getRelationsById('roles', this.priVar.id, 'permissions').toPromise(),
        // this.permissionsService.role.getOneByIdWithPermissions(this.priVar.id).toPromise(),
        this.fofGenericService.endpoints.search('permissions', {limit: 1000, sort: 'code', direction: 'ASC' }).toPromise()
      ])
      .then(result => {       
        const role:iRole = result[0]
        const rolePermissions:iRolePermission[] = result[1].data
        const permissions: iPermission[] = <any>result[2].data

        console.log('rolePermissions', rolePermissions)

        if (!role) {            
          this.fofNotificationService.error(`Role doesn't exist`)
          this.router.navigate(['../'], {relativeTo: this.activatedRoute})
          return
        }

        // console.log('permissions', permissions.map(e => e.code))

        this.uiVar.role = role
        this.uiVar.form.patchValue(this.uiVar.role)

        const _permissions: any[] = []
        let _permission: any          
        let previousNameRoot: string | null= null
        
        permissions.forEach((permission: iPermission) => {          
          //split to upper case
          const names = permission.code.split(/(?=[A-Z])/)
          const nameRoot = names[0]

          let found:any = false
          let permissionsLinked: iRolePermission | undefined = undefined
          
          
          if (role) {
            permissionsLinked = rolePermissions?.find(item => item.id == permission.id)
            if (permissionsLinked) { 
              found = true 
            }
          }  

          const permissionCode = permission.code.slice(nameRoot.length)

          
          if (nameRoot === previousNameRoot) {
            _permission.children.push({
              id: permission.id,                
              checked: found,
              permissionsLinked: permissionsLinked,
              code: permissionCode
            })
          } else {
            previousNameRoot = nameRoot
            if (_permission) { _permissions.push(_permission)}
            _permission = {}         
            _permission.code = nameRoot
            _permission.children = []
            _permission.children.push({
              id: permission.id,
              checked: found,
              permissionsLinked: permissionsLinked,
              code: permissionCode
            })
          }          
        })
        if (_permission) { _permissions.push(_permission)}

        this.uiVar.permissionGroups = _permissions          
        this.uiVar.loading = false

        // console.log('this.uiVar.permissionGroups', this.uiVar.permissionGroups)

      })
      .catch(reason => {
        console.log('error reason', reason)
      })
    }
  }
  // All variables shared with UI 
  public uiVar = {
    title: 'New role',
    permissionGroups: <any>undefined,
    loading: false,
    roleIsNew: false,
    role: <iRole><unknown>undefined,
    form: this.formBuilder.group({      
      code: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', [Validators.maxLength(200)]]      
    }),
    permissions: {
      roleCreate: false,
      roleUpdate: false,
      roleDelete: false
    }
  }
  // All actions shared with UI 
  public uiAction = {
    roleSave:() => {      
      const roleToSave: iRole = this.uiVar.form.value

      if (!this.uiVar.form.valid) {
        this.fofNotificationService.error('Please, correct the mistakes before saving')
        fofUtilsForm.validateAllFields(this.uiVar.form)
        return
      }

      if (roleToSave.code.toLowerCase() == 'new') {
        this.fofNotificationService.error('SOrry, that code is not free')
        return
      }

      if (this.uiVar.roleIsNew) { 
        this.fofGenericService.endpoints.create('roles', roleToSave)               
        .toPromise()
        .then((newRole: iRole) => {
          this.fofNotificationService.success('Role updated', {mustDisappearAfter: 1000})
          this.priVar.id = newRole.id!
          this.uiVar.title = 'Update role'
          this.uiVar.roleIsNew = false
          this.privFunc.roleLoad()                
        })
        .catch(reason => {
          this.fofErrorService.errorManage(reason)
        })
      } else {
        roleToSave.id = this.uiVar.role.id 
      
        this.fofGenericService.endpoints.update('roles', roleToSave)               
        .toPromise()
        .then((result: any) => {
          this.fofNotificationService.success('Role updated', {mustDisappearAfter: 1000})
        }) 
      }
    },
    roleCancel:() => {
      this.uiVar.permissionGroups = null      
      this.privFunc.roleLoad()
    },
    roleDelete:() => {
      this.fofDialogService.openYesNo({        
        question: 'Do you really want to delete the role?'
      }).then(yes => {
        if (yes) {
          this.fofGenericService.endpoints.delete('roles', this.uiVar.role)          
          .toPromise()
          .then(result => {
            this.fofNotificationService.success('Role deleted')
            this.router.navigate(['../'], {relativeTo: this.activatedRoute})
          }) 
        }
      })
    },
    rolePermissionSave: (permissionUI: any, $event: any) => {

      if (!this.base.permissions.update) {        
        $event.preventDefault()
        this.fofNotificationService.warn(`You don't have the necessary permissions`)
        return
      }

      const permission = permissionUI.permissionsLinked 

      // The roles must be deleted
      if (permission) {        
        const linkToDelete = permission.roles[0]       
        this.permissionsService.role.deleteLinkWithPermission(linkToDelete.roleId, linkToDelete.permissionId)
        .toPromise()
        .then((result: any) => {          
          permission.roles = null
          this.fofNotificationService.saveIsDone()
        })
      } else {        
        this.permissionsService.role.createLinkWithPermission(<any>this.uiVar.role.id, permissionUI.id)
        .toPromise()
        .then((result: any) => {          
          this.priVar.permissionRoluUpdating = false
          permissionUI.roles = result
          this.fofNotificationService.saveIsDone()
        })
      }      
    }
  }
  // Angular events
  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      const id = params.get('code')
      this.priVar.id = <any>id
      
      if (id) {
        if (id.toLowerCase() == 'new') {
          this.uiVar.roleIsNew = true
        } else {
          this.uiVar.title = 'Update role'
          this.privFunc.roleLoad()
        }
      }      
    })
  }

  ngOnDestroy() {
    this.priVar.destroy$.next()    
  }
}
