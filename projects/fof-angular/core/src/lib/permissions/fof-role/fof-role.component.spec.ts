import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FofRoleComponent } from './fof-role.component';

describe('FofRoleComponent', () => {
  let component: FofRoleComponent;
  let fixture: ComponentFixture<FofRoleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FofRoleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FofRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
