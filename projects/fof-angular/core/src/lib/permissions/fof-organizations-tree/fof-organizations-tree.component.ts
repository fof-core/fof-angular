import { Component, EventEmitter, Output, Input, OnInit, OnChanges } from '@angular/core'
import { NestedTreeControl } from '@angular/cdk/tree'
import { MatTreeNestedDataSource } from '@angular/material/tree'
import { iOrganization } from '../permissions.interface'
import { PermissionsService } from '../permissions.service'
import { FofErrorService } from '../../core/fof-error.service'

export interface iOrgUI extends iOrganization {
  checked?: boolean,
  indeterminate?: boolean,
  notSelectable?: boolean,
  children?: iOrgUI[],
  noAccess?: boolean
}

@Component({
  selector: 'fof-organizations-tree',
  templateUrl: './fof-organizations-tree.component.html',
  styleUrls: ['./fof-organizations-tree.component.scss']
})
export class FofOrganizationsTreeComponent implements OnInit {
  @Input() canSelectAncestors: boolean = true
  /** Change the current organizations */
  @Input() nodeChanged!: iOrgUI | null
  /** Notifiy the tree to delete an organizations */
  @Input() nodeToDelete!: iOrgUI | null
  /** Can the user could select multiple organizations? */ 
  @Input() multiSelect: boolean = false  
  /** Organization we want to be already checked */
  @Input() selectedNodesSynchronize!: iOrgUI[]
  /** Display a refresh tree button? */
  @Input() refreshButtonDisplay: boolean = false
  /** Prevent to check some organizations */
  @Input() notSelectableOrganizations!: iOrgUI[]
  /** Submit all the organization selected everytime an orgnization is selected or unselected 
   * AN objet or array, depending from the multiSelect param
  */
  @Output() selectedOrganizationsChange: EventEmitter<iOrgUI | iOrgUI[]> = new EventEmitter()

  constructor(
    private fofPermissionService: PermissionsService,
    private fofErrorService: FofErrorService
  ) {     
    this.uiAction.organizationTreeLoad()
  }

  // All private variables
  private priVar = {
    currentNode: <iOrganization><unknown>undefined,
    selectedNodesSynchronizeSave: <iOrgUI[]><unknown>undefined
  }
  // All private functions
  private privFunc = {    
    selectedNodeSynchronize: () => {
      const tree = this.uiVar.organizations.dataSource.data
      const selectedNodesSynchronize = this.selectedNodesSynchronize
      let areSomeNodeNotSelectable = false

      if (this.notSelectableOrganizations && this.notSelectableOrganizations.length > 0) {
        areSomeNodeNotSelectable = true
      }

      const getSelectedNode = (node: iOrgUI) => {

        if (selectedNodesSynchronize) {
          
          const foundedNode =  selectedNodesSynchronize.filter(n => {
            if (n.id === node.id) {
              // ugly fix: toDo: to review
              if (!n.name) {n.name = node.name}
              return node
            }
            return false
          })

          if (foundedNode.length > 0) {
            node.checked = true          
          } else {
            node.checked = false
          }

          // if (selectedNodesSynchronize && selectedNodesSynchronize.filter(n => n.id === node.id).length > 0) {
          //   node.checked = true          
          // } else {
          //   node.checked = false
          // }
        }        

        if (areSomeNodeNotSelectable) {
          if (this.notSelectableOrganizations.filter(n => n.id === node.id).length > 0) {
            // would prevent the user to check again the same organization 
            if (this.priVar.selectedNodesSynchronizeSave.filter(n => n.id === node.id).length === 0) {
              node.notSelectable = true
            } 
          } else {
            node.notSelectable = false
          }
        }

        node.children?.forEach(child => getSelectedNode(child))                
      }

      if (tree && tree.length > 0) {
        getSelectedNode(tree[0])
      }
    }
  }
  // All variables shared with UI 
  public uiVar = {
    loading: true,   
    organizations: {     
      treeControl: new NestedTreeControl<iOrgUI>(node => node.children),
      dataSource: new MatTreeNestedDataSource<iOrgUI>()
    },   
  }
  // All actions shared with UI 
  public uiAction = {
    OrganizationNodeHasChild: (_: number, node: iOrgUI) => !!node.children && node.children.length > 0,    
    organizationTreeLoad:(forceRefresh?: boolean) => {      
      this.uiVar.loading = true
      this.fofPermissionService.organization.getTreeView(forceRefresh)
      .toPromise()
      .then((organizations: iOrgUI[]) => {        
        this.uiVar.organizations.dataSource.data = organizations        
        if (organizations.length > 0) {          
          this.uiVar.organizations.treeControl.expand(organizations[0])     
          this.privFunc.selectedNodeSynchronize()
        }
      })
      .catch(reason => {
        this.fofErrorService.errorManage(reason)
      })
      .finally(() => {
        this.uiVar.loading = false
      })  
    },
    treeViewNodeSelect:(node: iOrgUI, $event: any) => {
      const tree = this.uiVar.organizations.dataSource.data
      
      const nodeUnSelect = (node: iOrgUI) => {
        node.indeterminate = false
        node.checked = false
        if (!node.children) { node.children = [] }
        node.children.forEach(child => nodeUnSelect(child))                
      }

      if (this.multiSelect) {
        const nodes: iOrganization[] = []

        node.checked = $event.checked

        const getSelectedNode = (node: iOrgUI) => {
          if (node.checked) {
            const selectNode = {...node}
            delete selectNode.children
            nodes.push(selectNode)
          }          
          node.children?.forEach(child => getSelectedNode(child))                
        }
  
        if (tree && tree.length > 0) {
          getSelectedNode(tree[0])
        }

        this.selectedOrganizationsChange.emit(nodes)

      } else {        

        if (tree && tree.length > 0) {
          nodeUnSelect(tree[0])
        }
  
        node.checked = $event.checked
        
        if (node.checked) {
          this.priVar.currentNode = node
        } else {
          // this.priVar.currentNode = null
          (this.priVar.currentNode as any) = null          
        }

        if (node.id == 'fake-root') {       
          this.selectedOrganizationsChange.emit(undefined)
          // nodeUnSelect(tree[0])
          return
        }

        this.selectedOrganizationsChange.emit(this.priVar.currentNode)
      }
    }
  }
  // Angular events
  ngOnInit() {
    
  }  
  ngOnChanges() {

    setTimeout(() => {
      if (this.nodeToDelete) {        
        const spliceNode = (node: iOrgUI) => {        
          if (node.id === this.nodeToDelete?.id) return null       
          node.children?.forEach((child, index) => {
            if (child.id === this.nodeToDelete?.id) { 
              node.children?.splice(index, 1) 
              return
            }
            spliceNode(child)
          })  
          return               
        }
  
        const data = this.uiVar.organizations.dataSource.data.slice()      
        spliceNode(data[0])
        // toDO: Performance   
        // bug on tree refresh
        // https://github.com/angular/components/issues/11381          
        // this.uiVar.organizations.dataSource.data = null
        this.uiVar.organizations.dataSource.data = []
        this.uiVar.organizations.dataSource.data = data
        this.nodeToDelete = null
  
        return
      }
  
      if (this.nodeChanged) { 
        this.priVar.currentNode = this.nodeChanged
        // toDO: Performance   
        // bug on tree refresh
        // https://github.com/angular/components/issues/11381        
        const data = this.uiVar.organizations.dataSource.data.slice()
        // this.uiVar.organizations.dataSource.data = null
        this.uiVar.organizations.dataSource.data = []
        this.uiVar.organizations.dataSource.data = data
        this.nodeChanged = null
      }
  
      if (this.selectedNodesSynchronize || this.notSelectableOrganizations) {
        if (!this.priVar.selectedNodesSynchronizeSave) {
          this.priVar.selectedNodesSynchronizeSave = this.selectedNodesSynchronize.slice()
        }
        this.privFunc.selectedNodeSynchronize()
      }
    }, 10)    
  }
}