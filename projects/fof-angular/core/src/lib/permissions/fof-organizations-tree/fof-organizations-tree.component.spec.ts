import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FofOrganizationsTreeComponent } from './fof-organizations-tree.component';

describe('FofOrganizationsTreeComponent', () => {
  let component: FofOrganizationsTreeComponent;
  let fixture: ComponentFixture<FofOrganizationsTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FofOrganizationsTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FofOrganizationsTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
