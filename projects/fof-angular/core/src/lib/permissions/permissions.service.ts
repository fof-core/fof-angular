import { HttpClient } from '@angular/common/http'
import { Inject, Injectable } from '@angular/core'
import { BehaviorSubject, Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { ifofSearch } from '../core/core.interface'
import { CORE_CONFIG, IFofConfig } from '../fof-config'
import { iOrganization, iRole, iUser, iUserRoleOrganization } from './permissions.interface'

// toDO: change all relations! -> must NOT use an id
export interface iOrgUi extends iOrganization {
  fullPath?: string,
  isRoot?: boolean
}

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  constructor(
    private httpClient: HttpClient,
    @Inject(CORE_CONFIG) private conf: IFofConfig
  ) { }

  private priVar = { 
    _organizations:<iOrganization[]><unknown>undefined,
    _organizationReadySubject: new BehaviorSubject<boolean>(false)
  }

  public organization = {
    treeViewReady$: this.priVar._organizationReadySubject.asObservable(),
    create:(organization: iOrganization) => 
      this.httpClient.post<iOrganization>(`${this.conf.environment.apiPath}/organizations`, organization),
    update:(organization: iOrganization) => 
      this.httpClient.patch<iOrganization>(`${this.conf.environment.apiPath}/organizations/${organization.id}`, organization),
    delete:(organization: iOrganization) => 
      this.httpClient.delete<iOrganization>(`${this.conf.environment.apiPath}/organizations/${organization.id}`),
    getTreeView:(forceRefresh?: boolean): Observable<iOrganization[]> => {  
      if (this.priVar._organizations && !forceRefresh) {
        // deep copy in case there is 2 instances of the component in the same page                
        return of(JSON.parse(JSON.stringify(this.priVar._organizations)))
      }     
      return this.httpClient.get<iOrganization[]>(`${this.conf.environment.apiPath}/organizations/getTreeView`) 
        .pipe(
          map((organizations: any) => {
            this.organization.helpers.setTreeview(organizations)            
            return organizations
          })
        )     
    },
    helpers: {
      cleanTreeview:(organizations: iOrgUi[]) => {
        const reset = (node: iOrgUi) => {          
          

          if (node.children && node.children.length > 0) {          
            node.children.forEach(child => reset(child))
          }

          if (organizations && organizations.length > 0) {          
            organizations[0].isRoot = true
            reset(organizations[0])
          }        
        }
      },
      setTreeview:(organizations: iOrgUi[]) => {
        
        const fillPath = (node: iOrgUi, fullPath: string) => {          
          node.fullPath = fullPath
          if (node.children && node.children.length > 0) {
            fullPath = fullPath + `${node.name}/`   
            node.children.forEach(child => fillPath(child, fullPath))
          }
        }

        if (organizations && organizations.length > 0) {          
          organizations[0].isRoot = true
          fillPath(organizations[0], '')
        }        

        this.priVar._organizations = organizations
        this.priVar._organizationReadySubject.next(true)
      }
    },   
    node: {
      getOneById:(nodeId: string): iOrgUi => {
        let foundedNode: iOrganization  = {}      
        const getNode = (node: iOrganization) => {          
          if (node.id === nodeId) {
            foundedNode = node            
          }
          if (!foundedNode) {
            if (node.children) {
              node.children.forEach(child => getNode(child))
            }            
          }          
        }

        if (this.priVar._organizations && this.priVar._organizations.length > 0) {
          getNode(this.priVar._organizations[0])
        }
        return foundedNode
      }
    }
  }

  public role = {  
    getAll: (serviceBackUrl?: string): Observable<iRole[]> => 
      this.httpClient.get<Array<iRole>>(`${serviceBackUrl || this.conf.environment.apiPath}/roles`),   
    deleteLinkWithPermission:(roleId: string, permissionId: string) =>  {
      return this.httpClient.delete<ifofSearch<iRole>>(`${this.conf.environment.apiPath}/roles/${roleId}/permissions/${permissionId}`)
    },
    createLinkWithPermission:(roleId: string, permissionId: string) =>  {
      const dto = {
        permissionId: permissionId
      }
      return this.httpClient.post<ifofSearch<iRole>>(`${this.conf.environment.apiPath}/roles/${roleId}/permissions`, dto)
    },
    createLinkWithUserAndOrganization:(dto: any) =>  {      
      return this.httpClient.post<ifofSearch<iRole>>(`${this.conf.environment.apiPath}/roles/users/organizations/bulk`, dto)
    }
  }

  public user = {   
    replaceUserRoleOrganization:(userRoleOrganizations: iUserRoleOrganization[], 
      userId: string, organisationId: string, serviceBackUrl?: string) => 
        this.httpClient.put<iUserRoleOrganization[]>(`${serviceBackUrl || 
          this.conf.environment.apiPath}/users/${userId}/organization/${organisationId}/roles`, userRoleOrganizations),          
    deleteUserRoleOrganizations:(userId: string, organisationsId: string[], serviceBackUrl?: string) => 
      this.httpClient.delete<iUserRoleOrganization[]>(`${serviceBackUrl || 
          this.conf.environment.apiPath}/users/${userId}/organizations?ids=${JSON.stringify(organisationsId)}`),
    getWithRoleById:(id: string, serviceBackUrl?: string): Observable<iUser> => {
      return this.httpClient.get<iUser>(`${serviceBackUrl || this.conf.environment.apiPath}/users/${id}/roles`)      
    }
  }
}
