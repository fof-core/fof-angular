import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core'
import { iOrgUi } from '../permissions.service'
import { FofTableComponent } from '../../components/fof-table/fof-table.component'
import { BaseService } from '../../core/base/base.service'
import { BaseComponent } from '../../core/base/component.base'
import { eCp } from '../permission.enum'
import { iOrganization } from '../permissions.interface'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'core-fof-users',
  templateUrl: './fof-users.component.html',
  styleUrls: ['./fof-users.component.scss']
})
export class FofUsersComponent extends BaseComponent implements OnInit {
  @ViewChild(FofTableComponent) fofTableComponent!: FofTableComponent<eCp>


  constructor(
    private baseService: BaseService
  ) { 
    super(baseService, {
      permissions: {
        create: eCp.roleCreate
      }
    })
  }
  // All private variables
  private priVar = {        
    filterOrganizationsChange: <EventEmitter<iOrganization>>new EventEmitter(),
    filterOrganizations!: <string[] | null>[<unknown>null]
  }
  public uiVar = {
    additionalSearchParam!:<string><unknown>undefined
  }
  // All actions shared with UI 
  public uiAction = {
    organisationMultiSelectedChange:(organizationParam: iOrgUi | iOrgUi[] ) => {      
      
      if (!organizationParam) {
        this.uiVar.additionalSearchParam = ''
        this.priVar.filterOrganizationsChange.emit(<any>null)
        this.uiVar.additionalSearchParam = ''
        return
      }

      let organization:iOrgUi 
      if (Array.isArray(organizationParam)) {
        organization = organizationParam[0]
      } else {
        organization = organizationParam
      }
      if (organization) {
        this.priVar.filterOrganizations = [organization.id || '']
      } else {
        this.priVar.filterOrganizations = null
      }    
      // console.log('organization', organization)  
      this.priVar.filterOrganizationsChange.emit(organization)
      if (organization.id) {
        this.uiVar.additionalSearchParam = `users.organizationId||$eq||${organization.id}` 
      }      
    }
  }
  ngAfterViewInit() {  

    // this.priVar.filterOrganizationsChange
    // .pipe(takeUntil(this.base.destroy$))
    // .subscribe( result => {
    //   console.log('result', result)
    // })
      
    this.fofTableComponent.config({
      endPoint: 'users',  
      routeParam: 'id',      
      // columnAuto: true,
      columns: {
        def: [
          {technicalName: 'organization.name', displayedName:'Organization', sortable: true},
          {technicalName: 'firstName', displayedName:'First name', sortable: true},
          {technicalName: 'lastName', displayedName:'Last name', sortable: true},
          {technicalName: 'eMail', displayedName:'eMail', sortable: true}          
        ],
        // default: ['custom'],
        default: ['organization.name', 'firstName', 'lastName', 'eMail'],
        smallDevice: ['organization.name', 'firstName', 'lastName']
      }, 
      defaultSort: 'lastName',
      fullTextSearchQUery: `users.firstName ||$cont|| $value
                      ||$or|| users.lastName ||$cont|| $value
                      ||$or|| users.eMail ||$cont|| $value
                      ||$or|| organization.name ||$cont|| $value`      
    })   

  }
  ngOnInit(): void {
  }
}
