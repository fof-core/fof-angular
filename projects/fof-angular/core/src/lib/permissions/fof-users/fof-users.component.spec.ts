import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FofUsersComponent } from './fof-users.component';

describe('FofUsersComponent', () => {
  let component: FofUsersComponent;
  let fixture: ComponentFixture<FofUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FofUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FofUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
