import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { FofOrganizationsComponent } from './fof-organizations/fof-organizations.component'
import { FofRolesComponent } from './fof-roles/fof-roles.component'
import { FofRoleComponent } from './fof-role/fof-role.component'
import { FofAuthGuard } from '../auth/auth.guard'
import { eCp } from './permission.enum'
import { FofUsersComponent } from './fof-users/fof-users.component'
import { FofUserComponent } from './fof-user/fof-user.component'

const routes: Routes = [ 
  { path: 'admin', canActivate: [FofAuthGuard], data: {permissions: [eCp.organizationRead]},
    children: [
      { path: 'organization', component: FofOrganizationsComponent },
      // { path: 'roles', component: FofRolesComponent }
      { path: 'roles', 
        children: [
          {path: '', component: FofRolesComponent},
          {path: ':code', component: FofRoleComponent}
        ]
      },
      { path: 'users', 
        children: [
          {path: '', component: FofUsersComponent},
          {path: ':code', component: FofUserComponent}
        ]
      }
    ]
  }  
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermissionsRoutingModule { }
