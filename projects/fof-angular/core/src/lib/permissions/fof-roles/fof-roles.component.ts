import { Component, ViewChild, AfterViewInit, OnDestroy, AfterContentInit } from '@angular/core'
import { PermissionsService } from '../permissions.service'
import { eCp } from '../permission.enum'
import { FofTableComponent } from '../../components/fof-table/fof-table.component'
import { BaseComponent } from '../../core/base/component.base'
import { BaseService } from '../../core/base/base.service'

@Component({
  selector: 'fof-roles',
  templateUrl: './fof-roles.component.html',
  styleUrls: ['./fof-roles.component.scss'],  
})
export class FofRolesComponent extends BaseComponent implements AfterViewInit, OnDestroy, AfterContentInit {
  @ViewChild(FofTableComponent) fofTableComponent!: FofTableComponent<eCp>

  constructor(      
    private baseService: BaseService
  ) { 
    super(baseService, {
      permissions: {
        create: eCp.roleCreate
      }
    })   
  }
 
  // All private variables
  private priVar = {    
    
  }
  // All private functions
  private privFunc = {
  }
  // All variables shared with UI 
  public uiVar = {    
    
  }
  // All actions shared with UI 
  public uiAction = {
  }

  ngAfterContentInit() {
    
  }
  // Angular events
  ngAfterViewInit() {  
    
    this.fofTableComponent.config({
      endPoint: 'roles',  
      routeParam: 'id',      
      columns: {
        def: [
          {technicalName: 'code', displayedName:'Code', sortable: true},
          {technicalName: 'description', displayedName:'Description'}
        ],
        default: ['code', 'description'],
        smallDevice: ['code']
      }, 
      defaultSort: 'code',
      fullTextSearchQUery: `role.code||$cont||$value`      
    })   

  }
  ngOnDestroy(): void {
   
  }

}
