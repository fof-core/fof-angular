import { Component, OnInit, ViewChild, AfterViewInit, 
  OnChanges, ContentChildren, QueryList, Input } from '@angular/core'
// import { PermissionsService } from '../permissions.service'
import { FofNotificationService } from '../../core/notification/notification.service'
// import { iRole, iUser, iUserLogin } from '../permissions.interface'
import { MatPaginator } from '@angular/material/paginator'
import { MatSort } from '@angular/material/sort'
import { merge, of as observableOf } from 'rxjs'
import { catchError, map, startWith, switchMap, take, takeUntil } from 'rxjs/operators'
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout'
import { BaseComponent } from '../../core/base/component.base'
import { BaseService } from '../../core/base/base.service'
import { MatColumnDef, MatTable } from '@angular/material/table'
import { FofGenericService } from '../generic.service'
import { FormControl } from '@angular/forms'
import { ActivatedRoute } from "@angular/router"
import { Router } from "@angular/router"

// https://stackoverflow.com/questions/53335929/content-projection-inside-angular-material-table
// https://github.com/angular/components/blob/34dd468158ff11b95658c2a5ea662c4cd16afe68/src/material/sort/sort-header.ts

export interface iFofTableConfig {
  /** The method used to search  
   * 
   * @important 
   * API must be a part of the fof framework
  */ 
  endPoint: string,  
  columns: {
    def: iFofTableColum[],
    default: string[]    
    smallDevice?: string[]
  }  
  routeParam?: string,
  defaultSort?: string,  
  fullTextSearchQUery?: string,
  columnAuto?: boolean
}

export interface iFofTableColum {
  technicalName: string,
  displayedName?: string,
  sortable?: boolean,
  nested?: boolean,
  customHtml?: boolean
}

enum eFilters {
  fullSearch = "fullSearch",
  sort = "sort"
}

@Component({
  selector: 'fof-table',
  templateUrl: './fof-table.component.html',
  styleUrls: ['./fof-table.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class FofTableComponent<T> extends BaseComponent implements OnInit, AfterViewInit, OnChanges   {
  @ViewChild(MatPaginator) paginator!: MatPaginator
  @ViewChild(MatSort) sort!: MatSort
  @ContentChildren(MatColumnDef) columnDefs!: QueryList<MatColumnDef>
  @ViewChild(MatTable, { static: true }) table!: MatTable<T>

  @Input() linkParams!: string
  @Input() sortActive!: string
  @Input() columnAuto:boolean = false
  @Input() additionalSearchParam:string | null = null

  constructor(   
    private fofNotificationService: FofNotificationService,
    private breakpointObserver: BreakpointObserver,
    private baseService: BaseService,
    private fofGenericService: FofGenericService,
    private activatedRoute: ActivatedRoute,		
		private router: Router  
  ) { 
    super(baseService)
    this.priVar.activatedRoute = activatedRoute
  }

  // All private variables
  private priVar = {        
    config: <iFofTableConfig | null>null,
    additionalSearchParam!:<string | null>null,
    activatedRoute: <ActivatedRoute | null>null    
  }
  // All private functions
  private privFunc = {
    columnsExtract:(dataset: any[]) => {
      if (dataset && dataset.length > 0) {
        const record = dataset[0]  

        const iterateThrowRecord = (recordItem: Object, letfPart?: string) => {
          const columns =  Object.getOwnPropertyNames(recordItem)
          
          for (let index = 0; index < columns.length; index++) {
            const property = columns[index]
           
            if (record[property] && typeof record[property] == 'object') { 
                // this is a nested object
                iterateThrowRecord(record[property], property)                
             
            } else {
              this.uiVar.allColumns.push({
                technicalName: letfPart? `${letfPart}.${property}`: property, 
                displayedName: letfPart? `${letfPart}.${property}`: property, 
                sortable: true
              })
            }            
            
          }
        }

        if (this.columnAuto) {          
          this.uiVar.allColumns = []
          iterateThrowRecord(record)         
          this.uiVar.displayedColumns = this.uiVar.allColumns.map(c => c.technicalName)
        }
        
      }      
    },
    applyFilterToRoute:(name: string, value: string) => {
      
      this.router.navigate([],
        {
          relativeTo: this.activatedRoute,
          queryParams: {[name]: value},
          queryParamsHandling: 'merge',          
          // NOTE: By using the replaceUrl option, we don't increase the Browser's
          // history depth with every filtering keystroke. This way, the List-View
          // remains a single item in the Browser's history, which allows the back
          // button to function much more naturally for the user.
          replaceUrl: true
        }
      )      
      // this.baseService.title.setTitle(`User. Search: ${ filter }`)         
    }
  }
  // All variables shared with UI 
  public uiVar = {    
    displayedColumns: <string[]>[],
    allColumns: <iFofTableColum[]>[],
    data: <any[]>[],
    resultsLength: 0,
    pageSize: 10,
    isLoadingResults: true ,
    searchUsersControl: new FormControl(),
  }
  // All actions shared with UI 
  public uiAction = {
    search:() => {
      this.privFunc.applyFilterToRoute(eFilters.fullSearch, this.uiVar.searchUsersControl.value)
      this.sort.sortChange.next()
    }
  }

  config(config: iFofTableConfig)  {
    setTimeout(() => {            
      this.uiVar.displayedColumns = config.columns.default  
      this.priVar.config = config
      this.uiVar.allColumns = config.columns.def.map(item => {
        item.nested = item.technicalName.split('.').length > 1
        return item
      })
      this.linkParams = config.routeParam || ''
      // this.sortActive = this.sortActive || config.defaultSort || ''      
      this.columnAuto = config.columnAuto || false
      
      this.sort.sortChange.next()
    }, 15)    
  }

  // Angular events
  ngOnInit() {
    this.breakpointObserver.observe(Breakpoints.XSmall)
    .pipe(takeUntil(this.base.destroy$))
    .subscribe((state: BreakpointState) => {
      if (state.matches) {
        // XSmall
        if (this.priVar.config?.columns.smallDevice) {
          this.uiVar.displayedColumns = this.priVar.config?.columns.smallDevice
        } else {
          if (this.priVar.config?.columns) {
            this.uiVar.displayedColumns = this.priVar.config?.columns.default
          }          
        }
      } else {
        // > XSmall        
        if (this.priVar.config?.columns.default) {
          this.uiVar.displayedColumns = this.priVar.config?.columns.default
        } else {
          if (this.priVar.config?.columns) {
            this.uiVar.displayedColumns = this.priVar.config?.columns.default
          }          
        }
      }
    })
  }  

  ngAfterContentInit() {
    
  }

  ngAfterViewInit() {  
    
    setTimeout(() => {
      this.activatedRoute.queryParams
        .pipe(take(1))
          .subscribe(params => {              
            // Search          
            this.uiVar.searchUsersControl.setValue(params[eFilters.fullSearch] || '')  
            // Sort
            if (params.sort) {
              const sorts = (params[eFilters.sort] as string).split(',')
              if (sorts.length == 2) {
                this.sort.active = sorts[0]
                this.sort.direction = <any>sorts[1]
              }              
            }
            this.sortActive = params.sort
            // fire refresh
            this.sort.sortChange.next()              
          }
        )
    }, 15)    
    
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange
      .pipe(takeUntil(this.base.destroy$))
      .subscribe((sort) => {
        if (!sort) { return }        
        this.privFunc.applyFilterToRoute(eFilters.sort, `${sort.active},${sort.direction}`)
        this.paginator.pageIndex = 0
      })

    setTimeout(() => {
      merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.uiVar.isLoadingResults = true          
          this.uiVar.pageSize = this.paginator.pageSize

          if (this.priVar.config && this.priVar.config.endPoint) {

            return  this.fofGenericService.endpoints.search(this.priVar.config.endPoint, {
              direction: this.sort.direction,
              limit: this.uiVar.pageSize,
              page: this.paginator.pageIndex+1,
              search: this.uiVar.searchUsersControl.value,
              sort: this.sort.active,              
              fullTextSearchQUery: this.priVar.config.fullTextSearchQUery,
              additionalSearchParam: this.additionalSearchParam==undefined?null:this.additionalSearchParam
            }).pipe(
              catchError((err) => {                
                console.log('toDO: manage error fof table', err)
                this.uiVar.isLoadingResults = false                
                return observableOf([])
              })
            )
          }
          return observableOf([])
        }),
        map((search: any) => {            
          this.privFunc.columnsExtract(search.data)                    
          this.uiVar.isLoadingResults = false
          this.uiVar.resultsLength = search.total
          return search.data
        }),
        catchError(() => {
          this.uiVar.isLoadingResults = false     
          console.log('Must reload the page')     
          return observableOf([])          
        })
      )
      .pipe(takeUntil(this.base.destroy$))
      .subscribe(data => this.uiVar.data = data)
    }, 30)
    
  }
  ngOnChanges() {
    
    if (this.additionalSearchParam) {
      if (this.priVar.additionalSearchParam != this.additionalSearchParam) {
        this.priVar.additionalSearchParam = this.additionalSearchParam            
      }
    }
    if (this.sort && this.sort.sortChange) { this.sort.sortChange.next() }
    
  }
  ngOnDestroy() {
    this.destroy()  
  }

}

