import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FofSearchPersistanceComponent } from './fof-search-persistance.component';

describe('FofSearchPersistanceComponent', () => {
  let component: FofSearchPersistanceComponent;
  let fixture: ComponentFixture<FofSearchPersistanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FofSearchPersistanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FofSearchPersistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
