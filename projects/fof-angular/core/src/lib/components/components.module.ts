import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FofTableComponent } from './fof-table/fof-table.component'
import { MaterialModule } from '../core/material.module'
import { RouterModule } from '@angular/router';
import { FofSearchBarComponent } from './fof-search-bar/fof-search-bar.component'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { FofGenericService } from './generic.service';
import { FofSearchPersistanceComponent } from './fof-search-persistance/fof-search-persistance.component'

@NgModule({

  declarations: [
    FofTableComponent,
    FofSearchBarComponent,
    FofSearchPersistanceComponent
  ],
  providers: [
    FofGenericService
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
    FofTableComponent,
    FofSearchBarComponent
  ]
})
export class ComponentsModule { }
