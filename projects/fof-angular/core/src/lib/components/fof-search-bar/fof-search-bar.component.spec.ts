import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FofSearchBarComponent } from './fof-search-bar.component';

describe('FofSearchBarComponent', () => {
  let component: FofSearchBarComponent;
  let fixture: ComponentFixture<FofSearchBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FofSearchBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FofSearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
