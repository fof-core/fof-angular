import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators'

@Component({
  selector: 'fof-search-bar',
  templateUrl: './fof-search-bar.component.html',
  styleUrls: ['./fof-search-bar.component.scss']
})
export class FofSearchBarComponent implements OnInit {
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fieldCtrl = new FormControl();
  filteredValues: Observable<string[]>;
  // fields: string[] = ['Lemon'];
  fields: string[] = [];
  allFIelds: string[] = ['code', 'description'];
  allOperators: string[] = ['=', '!=', 'startBy', 'contains', 'endBy']

  @ViewChild('fieldInput') fieldInput!: ElementRef<HTMLInputElement>;

  constructor() {
    this.filteredValues = this.fieldCtrl.valueChanges.pipe(
        startWith(null),
        map((field: string | null) => field ? this._filter(field) : this.allFIelds.slice()));
  }

  ngOnInit(): void {
  
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our field
    if (value) {
      this.fields.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();

    this.fieldCtrl.setValue(null);

  }

  remove(field: string): void {
    const index = this.fields.indexOf(field);

    if (index >= 0) {
      this.fields.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fields.push(event.option.viewValue);
    // this.fieldInput.nativeElement.value = '';
    this.fieldCtrl.setValue(null);

    this.filteredValues = this.fieldCtrl.valueChanges.pipe(
      startWith(null),
      map((field: string | null) => {
        return field ? this._filterOp(field) : this.allOperators.slice()
      }));
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allFIelds.filter(field => field.toLowerCase().includes(filterValue));
  }

  private _filterOp(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allOperators.filter(field => field.toLowerCase().includes(filterValue));
  }
}
