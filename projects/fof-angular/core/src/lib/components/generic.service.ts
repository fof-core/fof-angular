import { HttpClient } from '@angular/common/http'
import { Inject, Injectable } from '@angular/core'
import { BehaviorSubject, Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { ifofSearch } from '../core/core.interface'
import { CORE_CONFIG, IFofConfig } from '../fof-config'

interface iGenericSearch {
  limit?: number, 
  page?: number, 
  sort?: string, 
  direction?: any,
  search?: string, 
  fullTextSearchQUery?: string,
  additionalSearchParam?: string | null
}

@Injectable({
  providedIn: 'root'
})
export class FofGenericService {

  constructor(
    private httpClient: HttpClient,
    @Inject(CORE_CONFIG) private conf: IFofConfig
  ) { }

  private priVar = { 
  }

  public endpoints = {
    getOneById:(endPoint: string, id: string) => this.httpClient.get<any>(`${this.conf.environment.apiPath}/${endPoint}/${id}`),
    getRelationsById:(endPoint: string, id: string, relation:string ) => 
      this.httpClient.get<any>(`${this.conf.environment.apiPath}/${endPoint}/${id}/${relation}`),
    create:(endPoint: string, dto: any) => this.httpClient.post<any>(`${this.conf.environment.apiPath}/${endPoint}`, dto),
    createBulk:(endPoint: string, dto: any) => this.httpClient.post<any>(`${this.conf.environment.apiPath}/${endPoint}/bulk`, dto),
    update:(endPoint: string, dto: any) => this.httpClient.patch<any>(`${this.conf.environment.apiPath}/${endPoint}/${dto.id}`, dto),
    delete:(endPoint: string, dto: any) => this.httpClient.delete<any>(`${this.conf.environment.apiPath}/${endPoint}/${dto.id}`),
    search: (endPoint: string, params: iGenericSearch): Observable<ifofSearch<any>> => {

      // filter=code||$cont||das

      let formatedParams = ''
      let filter = ''
      let filterSearch = '' 

      if (!params.limit) params.limit = 10
      if (!params.page) params.page = 1

      formatedParams = formatedParams + `limit=${params.limit}&page=${params.page}`

      
      if (params.search) {
        if (params.search.toLowerCase().startsWith('filter=')) {
          filter = '&' + params.search
        } else {
          if (params.fullTextSearchQUery) {
            filter = `&filter=${params.fullTextSearchQUery.replace(/\$value/g, params.search)}`
          }        
        }        
      }

      filter = filter.replace(/\r?\n|\r/g, '')
      filter = filter.replace(/ /g, '')
     
      let criterias = filter.split('||$or||')

      if (criterias.length > 1 && params.additionalSearchParam) {
        filter = ''
        criterias.map(item => {
          filter = filter + item + `;${params.additionalSearchParam}||$or||`
        })
        filter = filter.slice(0, filter.length - 7)       
      } else {
        if (params.additionalSearchParam) {  
          if (filter.toLowerCase().startsWith('&filter=')) {
            filter = formatedParams + `;${params.additionalSearchParam}`
          } else {
            filter = `&filter=${params.additionalSearchParam}`
          }
        }      
      }

      formatedParams = formatedParams + filter
     
      if (params.sort) { 
        formatedParams = formatedParams + `&sort=${params.sort},${params.direction}`
      }

      return this.httpClient.get<ifofSearch<any>>(`${this.conf.environment.apiPath}/${endPoint}?${formatedParams}`)
    }  
  }
}
