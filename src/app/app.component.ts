import { Component } from '@angular/core'
// import {  } from '@fof-angular/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'fof-angular';
}
