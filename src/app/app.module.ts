import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { environment } from '../environments/environment'

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

// fof-core
import { FofCoreModule, IFofConfig, PermissionsModule } from '@fof-angular/core'
import { CompanyDefaultModule } from '@fof-angular/company-default'
import { MaterialModule } from './material.module'

// your custom actions for access management
export enum eP {  
  // invoiceCreate = 'invoiceCreate',
  // invoiceUpdate = 'invoiceUpdate', 
  // invoiceRead = 'invoiceRead',
  // invoiceDelete = 'invoiceDelete'   
}


// see comments in the source code for detail
const appConfig: IFofConfig = {
  appName: {
    long: 'fof test',
    technical: 'fof-test'
  },
  mainTitle: {
    long: 'fof Test Application ',
    short: 'fof Test App'
  },
  environment: environment,
  permissions: eP,
  // your custom main navigation 
  navigationMain: [
    { url: '/collaborators', label: 'Collaborateurs', icon:'perm_contact_calendar' },
    { url: '/miniservices', label: 'Mini-services', icon:'developer_board' },
    { url: 'http://localhost:4216', label: 'Service de démonstration', icon: 'anchor', external: true, target: '_self' }    
  ]
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FofCoreModule.forRoot(appConfig),
    MaterialModule,
    PermissionsModule,
    CompanyDefaultModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
